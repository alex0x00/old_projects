#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "frameopengl.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_pushButtonOpen_clicked();

    void on_pushButtonClear_clicked();

    void on_pushButtonExit_clicked();

    void on_pushButtonApply_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
