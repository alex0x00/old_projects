#include "blockscheme.h"
#include <QPainter>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QGLWidget>
#include <math.h>

#define M_PI 3.14159265358979323846

AlgoritmBlockScheme::AlgoritmBlockScheme(AlgoritmBlockSchemeType type, const QVector2D& pos, const QString& text)
{
    m_texture = 0;
    m_length = m_angle = 0.0;
    if (type > 1) {
        m_type = type;
        m_position = pos;
        if (type == Begin && text.isEmpty()) {
            createTextureText(QString("Начало"));
        } else if (type == End && text.isEmpty()) {
            createTextureText(QString("Конец"));
        } else {
            createTextureText(text);
        }
        updateTransform();
    } else {
        m_type = Undefined;
        qDebug() << "incorrect type!";
    }
}

AlgoritmBlockScheme::AlgoritmBlockScheme(AlgoritmBlockSchemeType type, const QVector2D& pos, float angle, float length)
{
    m_texture = 0;
    m_length = m_angle = 0.0;

    if (type <= 1) {
        m_type = type;
        m_position = pos;
        m_length = (length < 0.5) ? 0.5 : length;
        m_angle = angle;
        updateTransform();
    } else {
        m_type = Undefined;
        qDebug() << "incorrect type!";
    }
}

QVector2D AlgoritmBlockScheme::position() const
{
    return m_position;
}

void AlgoritmBlockScheme::setPosition(const QVector2D& position)
{
    m_position = position;
    updateTransform();
}

void AlgoritmBlockScheme::createTextureText(const QString& text)
{
    //создаем текстуру с нашим изображением
    //ресуем на изображении
    QImage* image = new QImage(512, 512, QImage::Format_ARGB32);
    QRect rect(0, 0, image->width(), image->height());
    QPainter painter;
    QFont font;
    font.setPixelSize(64);
    painter.begin(image);
    painter.setBrush(QBrush(QColor(Qt::blue)));
    painter.drawRect(rect);
    painter.setFont(font);
    painter.setPen(QPen(QColor(Qt::green)));
    painter.drawText(rect, Qt::AlignCenter, text);
    painter.end();

    QImage prepare;
    //преобразуем изображение в формат для opengl
    prepare = QGLWidget::convertToGLFormat(*image);

    glGenTextures(1, &m_texture); //запрашиваем индекс текстуры
    glBindTexture(GL_TEXTURE_2D, m_texture); //используем этот текстурный
    //индекс  как текущий
    //передаем данные
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, prepare.width(), prepare.height(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, prepare.bits());
    //параметры фильтрации
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void AlgoritmBlockScheme::draw()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(m_transform.data());
    glColor3f(0.0, 0.0, 1.0);

    switch (m_type) {
    case AlgoritmBlockScheme::Begin:
        drawBeginEnd();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::End:
        drawBeginEnd();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::Function:
        drawBeginEnd();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::Conditional:
        drawConditional();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::Action:
        drawAction();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::Loop:
        drawLoop();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::InputOutput:
        drawInputOutput();
        drawBlockText();
        break;
    case AlgoritmBlockScheme::Arrow:
        drawArrow();
        break;
    case AlgoritmBlockScheme::Line:
        drawLine();
        break;
    }
}

void AlgoritmBlockScheme::updateTransform()
{
    m_transform.setToIdentity();
    m_transform.translate(m_position.x(), m_position.y());
    if (m_type <= 1) {
        m_transform.rotate(m_angle, QVector3D(0, 0, 1));
    }
}

void AlgoritmBlockScheme::drawLine()
{
    float size = 0.5 / 2.0;
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(0.0, size, size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    //левая грань
    glVertex3f(0.0, size, -size);
    glVertex3f(0.0, size, size);
    glVertex3f(0.0, -size, size);
    glVertex3f(0.0, -size, -size);
    // задняя грань
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(0.0, -size, -size);
    //правая грань
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(1.0 * m_length, -size, -size);

    //верх
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(0.0, size, size);
    //низ
    glVertex3f(0.0, -size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    glEnd();
}

void AlgoritmBlockScheme::drawArrow()
{
    drawLine();
    float size = 0.5;
    //0-1
    //3-2
    //
    glBegin(GL_QUADS);
    //левая грань
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(1.0 * m_length, -size, -size);

    //верх
    glVertex3f(1.0 * m_length + size, 0.0, -size);
    glVertex3f(1.0 * m_length + size, 0.0, size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, size, -size);
    //низ
    glVertex3f(1.0 * m_length + size, 0.0, -size);
    glVertex3f(1.0 * m_length + size, 0.0, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(1.0 * m_length, -size, -size);

    glBegin(GL_TRIANGLES);
    //ближняя грань
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length + size, 0.0, size);
    glVertex3f(1.0 * m_length, -size, size);

    // задняя грань
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length + size, 0.0, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glEnd();
}

void AlgoritmBlockScheme::drawBlockText()
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texture);
    //отрисовывает квадрат с текстурой

    glBegin(GL_QUADS);
    glVertex3f(-1.0, -1.0, 1.001);
    glTexCoord2f(0.01, 0.99);

    glVertex3f(-1.0, 1.0, 1.001);
    glTexCoord2f(0.99, 0.99);

    glVertex3f(1.0, 1.0, 1.001);
    glTexCoord2f(0.99, 0.01);

    glVertex3f(1.0, -1.0, 1.001);

    glTexCoord2f(0.01, 0.01);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

void AlgoritmBlockScheme::drawBeginEnd()
{
    float k1, k2, step = 10.0;

    glBegin(GL_TRIANGLES);
    for (float i = 90.0; i < 270.0; i += step) {
        //ближняя грань
        k1 = M_PI * i / 180.0;

        k2 = M_PI * (i + step) / 180.0;

        glVertex3f(cos(k1) - 1.0, sin(k1), 1.0);
        glVertex3f(-1.0, 0.0, 1.0);
        glVertex3f(cos(k2) - 1.0, sin(k2), 1.0);

        glVertex3f(cos(k1) - 1.0, sin(k1), -1.0);
        glVertex3f(-1.0, 0.0, -1.0);
        glVertex3f(cos(k2) - 1.0, sin(k2), -1.0);

        glVertex3f(cos(k1) - 1.0, sin(k1), 1.0);
        glVertex3f(cos(k2) - 1.0, sin(k2), 1.0);
        glVertex3f(cos(k1) - 1.0, sin(k1), -1.0);

        glVertex3f(cos(k1) - 1.0, sin(k1), -1.0);
        glVertex3f(cos(k2) - 1.0, sin(k2), -1.0);
        glVertex3f(cos(k2) - 1.0, sin(k2), 1.0);
    }
    for (float i = 270.0; i < 450.0; i += step) {

        //ближняя грань
        k1 = M_PI * i / 180.0;
        k2 = M_PI * (i + step) / 180.0;

        glVertex3f(cos(k1) + 1.0, sin(k1), 1.0);
        glVertex3f(1.0, 0.0, 1.0);
        glVertex3f(cos(k2) + 1.0, sin(k2), 1.0);

        glVertex3f(cos(k1) + 1.0, sin(k1), -1.0);
        glVertex3f(1.0, 0.0, -1.0);
        glVertex3f(cos(k2) + 1.0, sin(k2), -1.0);

        glVertex3f(cos(k1) + 1.0, sin(k1), 1.0);
        glVertex3f(cos(k2) + 1.0, sin(k2), 1.0);
        glVertex3f(cos(k1) + 1.0, sin(k1), -1.0);

        glVertex3f(cos(k1) + 1.0, sin(k1), -1.0);
        glVertex3f(cos(k2) + 1.0, sin(k2), -1.0);
        glVertex3f(cos(k2) + 1.0, sin(k2), 1.0);
    }
    glEnd();

    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    // задняя грань
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(-1.0, -1.0, -1.0);
    //верх
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    //низ
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glEnd();
}

void AlgoritmBlockScheme::drawInputOutput()
{
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(-1.5, 1.0, 1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(1.5, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    //левая грань
    glVertex3f(-1.5, 1.0, -1.0);
    glVertex3f(-1.5, 1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, -1.0);
    // задняя грань
    glVertex3f(-1.5, 1.0, -1.0);
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(1.5, -1.0, -1.0);
    glVertex3f(-2.0, -1.0, -1.0);
    //правая грань
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(1.5, -1.0, 1.0);
    glVertex3f(1.5, -1.0, -1.0);
    //верх
    glVertex3f(-1.5, 1.0, -1.0);
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(-1.5, 1.0, 1.0);
    //низ
    glVertex3f(-2.0, -1.0, -1.0);
    glVertex3f(1.5, -1.0, -1.0);
    glVertex3f(1.5, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    glEnd();
}

void AlgoritmBlockScheme::drawConditional()
{
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(-2.0, 0.0, 1.0);
    glVertex3f(0.0, 2.0, 1.0);
    glVertex3f(2.0, 0.0, 1.0);
    glVertex3f(0.0, -2.0, 1.0);
    //дальняя грань
    glVertex3f(-2.0, 0.0, -1.0);
    glVertex3f(0.0, 2.0, -1.0);
    glVertex3f(2.0, 0.0, -1.0);
    glVertex3f(0.0, -2.0, -1.0);
    //левая грань верх
    glVertex3f(0.0, 2.0, -1.0);
    glVertex3f(0.0, 2.0, 1.0);
    glVertex3f(-2.0, 0.0, 1.0);
    glVertex3f(-2.0, 0.0, -1.0);
    //левая грань низ
    glVertex3f(0.0, -2.0, -1.0);
    glVertex3f(0.0, -2.0, 1.0);
    glVertex3f(-2.0, 0.0, 1.0);
    glVertex3f(-2.0, 0.0, -1.0);

    //правая грань верх
    glVertex3f(0.0, 2.0, 1.0);
    glVertex3f(0.0, 2.0, -1.0);
    glVertex3f(2.0, 0.0, -1.0);
    glVertex3f(2.0, 0.0, 1.0);
    //правая грань низ
    glVertex3f(0.0, -2.0, 1.0);
    glVertex3f(0.0, -2.0, -1.0);
    glVertex3f(2.0, 0.0, -1.0);
    glVertex3f(2.0, 0.0, 1.0);
    glEnd();
}

void AlgoritmBlockScheme::drawAction()
{
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(-2.0, 1.0, 1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(2.0, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    //левая грань
    glVertex3f(-2.0, 1.0, -1.0);
    glVertex3f(-2.0, 1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, -1.0);
    // задняя грань
    glVertex3f(-2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(2.0, -1.0, -1.0);
    glVertex3f(-2.0, -1.0, -1.0);
    //правая грань
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(2.0, -1.0, 1.0);
    glVertex3f(2.0, -1.0, -1.0);

    //верх
    glVertex3f(-2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, -1.0);
    glVertex3f(2.0, 1.0, 1.0);
    glVertex3f(-2.0, 1.0, 1.0);
    //низ
    glVertex3f(-2.0, -1.0, -1.0);
    glVertex3f(2.0, -1.0, -1.0);
    glVertex3f(2.0, -1.0, 1.0);
    glVertex3f(-2.0, -1.0, 1.0);
    glEnd();
}

void AlgoritmBlockScheme::drawLoop()
{
    //0-1
    //3-2

    glBegin(GL_QUADS);
    //левый край
    //левая грань
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(1.0, -1.0, -1.0);

    //верх
    glVertex3f(1.0 + 1.0, 0.0, -1.0);
    glVertex3f(1.0 + 1.0, 0.0, 1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0, 1.0, -1.0);
    //низ
    glVertex3f(1.0 + 1.0, 0.0, -1.0);
    glVertex3f(1.0 + 1.0, 0.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(1.0, -1.0, -1.0);

    //правый край
    //левая грань
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, -1.0);

    //верх
    glVertex3f(-1.0 - 1.0, 0.0, -1.0);
    glVertex3f(-1.0 - 1.0, 0.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, -1.0);
    //низ
    glVertex3f(-1.0 - 1.0, 0.0, -1.0);
    glVertex3f(-1.0 - 1.0, 0.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, -1.0);
    glEnd();
    glBegin(GL_TRIANGLES);
    //ближняя грань
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0 + 1.0, 0.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);

    // задняя грань
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0 + 1.0, 0.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);

    //ближняя грань
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(-1.0 - 1.0, 0.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);

    // задняя грань
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(-1.0 - 1.0, 0.0, -1.0);
    glVertex3f(-1.0, -1.0, -1.0);
    glEnd();
    //куб
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    // задняя грань
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(-1.0, -1.0, -1.0);
    //верх
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(1.0, 1.0, 1.0);
    glVertex3f(-1.0, 1.0, 1.0);
    //низ
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, 1.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glEnd();
}
