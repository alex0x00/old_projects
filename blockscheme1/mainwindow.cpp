#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_W) {
        ui->widget->move(QVector3D(0, -1, 0));
    } else if (event->key() == Qt::Key_S) {
        ui->widget->move(QVector3D(0, 1, 0));
    } else if (event->key() == Qt::Key_A) {
        ui->widget->move(QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_D) {
        ui->widget->move(QVector3D(-1, 0, 0));
    }else if (event->key() == Qt::Key_Plus) {
        ui->widget->move(QVector3D(0, 0, -1));
    }else if (event->key() == Qt::Key_Minus) {
        ui->widget->move(QVector3D(0, 0, 1));
    }
}


void MainWindow::on_pushButtonOpen_clicked()
{
    //открывам диалог на выбор файла
    QString fileName = QFileDialog::getOpenFileName(this, tr("Открыть файл описания алгоритма"),
                                                    "./",
                                                    tr("Text (*.txt)"));
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Error algoritm file open:";
    } else {
        ui->widget->clearLoadedObjects();
        QString text(file.readAll());
        ui->plainTextEdit->setPlainText(text);
        ui->widget->loadAlgoritm(text);
    }
}

void MainWindow::on_pushButtonClear_clicked()
{
    ui->widget->clearLoadedObjects();
}

void MainWindow::on_pushButtonExit_clicked()
{
    exit(0);
}



void MainWindow::on_pushButtonApply_clicked()
{
    ui->widget->clearLoadedObjects();
    QString text(ui->plainTextEdit->toPlainText());
    ui->widget->loadAlgoritm(text);
}
