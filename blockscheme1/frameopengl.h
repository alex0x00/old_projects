#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtWidgets>
#include <QGLWidget>
#include "blockscheme.h"

class FrameOpenGL : public QGLWidget {
    Q_OBJECT

public:
    explicit FrameOpenGL(QWidget* parent = 0, QGLWidget* shareWidget = 0);
    ~FrameOpenGL();
    void loadAlgoritm(const QString& srcText);
    void clearLoadedObjects();
    void move(const QVector3D& pos);
    void rotate(float angle, const QVector3D& asix);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
private:
    void addObject(const AlgoritmBlockScheme::AlgoritmBlockSchemeType state,
                   const QVector2D& pos, const QString& text, float angle, float length);
    QMatrix4x4 m_projection;//матрица проекции
    QMatrix4x4 m_view;//матрица точки обзора
    QMatrix4x4 m_viewTransform;//матрица поворота камеры
    QVector<AlgoritmBlockScheme*> m_schemes;//все загруженные объекты
    float m_aspectRatio;//отножение ширины высоты
    QVector3D m_pos;//позиция камеры
    QVector3D m_center;//точка взгляда
};

#endif
