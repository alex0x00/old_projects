#include <QtWidgets>
#include <QtOpenGL>
#include "frameopengl.h"

FrameOpenGL::FrameOpenGL(QWidget* parent, QGLWidget* shareWidget)
    : QGLWidget(parent, shareWidget)
{
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4); //количество пикселей на сглаживание одного
    setFormat(fmt);
    m_pos = QVector3D(0, 0, 20);
    m_center = QVector3D(0, 0, 0);
}

FrameOpenGL::~FrameOpenGL()
{
    clearLoadedObjects();
}

void FrameOpenGL::initializeGL()
{
    //установка значениями
    m_viewTransform.setToIdentity();
    m_aspectRatio = width() / height();
    m_projection.perspective(60, m_aspectRatio, 1, 100);

    m_view.lookAt(m_pos,
                  m_center,
                  QVector3D(0, 1, 0));

    glEnable(GL_DEPTH_TEST);
}
//метод отрисовки
void FrameOpenGL::paintGL()
{
    glClearColor(1.0, 1.0, 1.0, 1.0); //серый фон
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    QMatrix4x4 vp;
    //устанавливаем результирующую матрицу проекции
    vp = m_projection * m_view * m_viewTransform;

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data()); //передам матрицу opengl
    glMatrixMode(GL_MODELVIEW);


    glLoadIdentity();
    //отрисовка объектов
    for (int i = 0; i < m_schemes.length(); ++i) {
        m_schemes[i]->draw();
    }
}

void FrameOpenGL::resizeGL(int width, int height)
{
    //изменяем настройки порта вывода графики
    glViewport(0, 0, width, height);
    m_aspectRatio = width / height;

    m_projection.setToIdentity();
    m_projection.perspective(60, m_aspectRatio, 1, 100);
    updateGL(); //обновляем
}

void FrameOpenGL::addObject(const AlgoritmBlockScheme::AlgoritmBlockSchemeType state, const QVector2D &pos, const QString &text, float angle, float length)
{
    //описаение объекта завершено, сохраняем
    AlgoritmBlockScheme* element = 0;
    switch (state) {
    case AlgoritmBlockScheme::Begin:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Begin, pos, text);
        break;
    case AlgoritmBlockScheme::End:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::End, pos, text);
        break;
    case AlgoritmBlockScheme::Function:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Function, pos, text);
        break;
    case AlgoritmBlockScheme::Conditional:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Conditional, pos, text);
        break;
    case AlgoritmBlockScheme::Action:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Action, pos, text);
        break;
    case AlgoritmBlockScheme::Loop:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Loop, pos, text);
        break;
    case AlgoritmBlockScheme::InputOutput:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::InputOutput, pos, text);
        break;
    case AlgoritmBlockScheme::Arrow:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Arrow, pos, angle, length);
        break;
    case AlgoritmBlockScheme::Line:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Line, pos, angle, length);
        break;
    }
    if (element != 0) {
        m_schemes.push_back(element); //добавляем новый элемент
    }
}

/*
 * void FrameOpenGL::loadAlgoritm(const QString& text)
{
    QString oneString;
    QStringList lines = text.split(QRegExp("(\\r\\n)|(\\n)+"), QString::SkipEmptyParts);
    QStringList words;
    QString text;
    QVector2D pos;
    float length;
    float angle;

    for (int i=0; i<lines.length();++i) {
        oneString = lines[i];
        words = oneString.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        if (words.length() == 0) {
 */


void FrameOpenGL::loadAlgoritm(const QString& srcText)
{
    QString oneString;
    QStringList lines = srcText.split(QRegExp("(\\r\\n)|(\\n)+"), QString::SkipEmptyParts);
    QStringList words;
    QString text;
    QVector2D pos;
    float length;
    float angle;

    AlgoritmBlockScheme::AlgoritmBlockSchemeType currentLoadedObject = AlgoritmBlockScheme::Undefined;

    for (int i=0; i<lines.length();++i) {
        oneString = lines[i];
        words = oneString.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        if (words.length() == 0) {
            continue; //если пустая строка
        }
        if (words[0] == "Начало") {
            text = "";
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Begin;
        } else if (words[0] == "Конец") {
            text = "";
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::End;
        } else if (words[0] == "Фунция" || words[0] == "Процедура") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Function;
            text = "";
        } else if (words[0] == "Условие") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Conditional;
            text = "";
        } else if (words[0] == "Действие") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Action;
            text = "";
        } else if (words[0] == "Цикл") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Loop;
            text = "";
        } else if (words[0] == "Ввод") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::InputOutput;
            text = "";
        } else if (words[0] == "Вывод") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::InputOutput;
            text = "";
        } else if (words[0] == "Стрелка") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Arrow;
            angle = length = 0.0;
        } else if (words[0] == "Линия") {
            addObject(currentLoadedObject, pos, text, angle, length);
            currentLoadedObject = AlgoritmBlockScheme::Line;
            angle = length = 0.0;
        } else if (words[0] == "позиция") {
            float x, y;
            x = words[1].toFloat();
            y = words[2].toFloat();
            pos.setX(x);
            pos.setY(y);
        } else if (words[0] == "описание" && currentLoadedObject > 1) {
            text = oneString.remove(0, QString("описание ").length()); // удаляем описание
        } else if (words[0] == "угол" && currentLoadedObject <= 1) {
            angle = words[1].toFloat();
        } else if (words[0] == "длина" && currentLoadedObject <= 1) {
            length = words[1].toFloat();
        }
    }
    addObject(currentLoadedObject, pos, text, angle, length);
    //qDebug() << "Load file algoritm \n Loading objects:" << m_schemes.length();
    updateGL(); //обновляем
}

void FrameOpenGL::clearLoadedObjects()
{
    for (int i=0; i< m_schemes.length(); ++i)
    {
        delete m_schemes[i];
    }
    m_schemes.clear();
    updateGL(); //обновляем
}

void FrameOpenGL::move(const QVector3D &pos)
{
    m_pos += pos;
    m_center += pos;
    m_view.setToIdentity();
    //формируем навую матрицу точки обзора
    m_view.lookAt(m_pos,
                  m_center,
                  QVector3D(0, 1, 0));
    updateGL(); //обновляем
}

void FrameOpenGL::rotate(float angle, const QVector3D &asix)
{
    m_viewTransform.rotate(angle, asix);
    m_view.setToIdentity();
    //формируем навую матрицу точки обзора
    m_view.lookAt(m_pos,
                  m_center,
                  QVector3D(0, 1, 0));
    updateGL(); //обновляем
}
