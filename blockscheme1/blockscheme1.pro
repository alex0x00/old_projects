HEADERS = \
        blockscheme.h \
    mainwindow.h \
    frameopengl.h
SOURCES = \
        main.cpp \
        blockscheme.cpp \
    mainwindow.cpp \
    frameopengl.cpp
QT += opengl widgets

LIBS += -lopengl32

FORMS += \
    mainwindow.ui
