#include "network.h"

ClientSocket *ClientSocket::instance()
{
    static ClientSocket* network = 0;
    if (network == 0)
    {
        WSADATA wsa_data;
        WORD version = MAKEWORD(2,2);
        int wsa_ret=WSAStartup(version, (LPWSADATA)&wsa_data);
        qDebug() << "init lib winsock2";
        if(wsa_ret!=0)
        {
            qDebug() << "error init lib winsock2";
        }
        network = new ClientSocket();
    }
    return network;
}

ClientSocket::~ClientSocket()
{
    if (m_isConnected){
        closesocket(m_socket);
    }
    WSACleanup();
}

bool ClientSocket::reConnect(QString host, unsigned int port)
{
    qDebug() << "init";

    if (m_isConnected){
        m_isConnected = false;
        closesocket(m_socket);
    }

    m_socket=socket(PF_INET, SOCK_STREAM, 0 /*IPPROTO_TCP*/);
    //Создаем сокет
    qDebug() << "get socket";
    if(m_socket == INVALID_SOCKET)
    {
        qWarning() << "Unable to create socket\n";

        int wsError = WSAGetLastError();// здесь обрабатываем ошибку
          switch (wsError)
          {
            case WSANOTINITIALISED:
              qDebug() << "socket WSANOTINITIALISED";
              break;
            case WSAENETDOWN:
              qDebug() << "socket WSAENETDOWN";
              break;
            case WSAEAFNOSUPPORT:
              qDebug() << "socket WSAEAFNOSUPPORT";
              break;
            case WSAEINPROGRESS:
              qDebug() << "socket WSAEINPROGRESS";
              break;
            case WSAEMFILE:
              qDebug() << "socket WSAEMFILE";
              break;
            case WSAENOBUFS:
              qDebug() << "socket WSAENOBUFS";
              break;
            case WSAEPROTONOSUPPORT:
              qDebug() << "socket WSAEPROTONOSUPPORT";
              break;
            case WSAEPROTOTYPE:
              qDebug() << "socket WSAEPROTOTYPE";
              break;
            case WSAESOCKTNOSUPPORT:
              qDebug() << "socket WSAESOCKTNOSUPPORT";
              break;
            default:
              break;
          }
    }

    LPHOSTENT hostEnt;
    qDebug() << "get host name";
    hostEnt = gethostbyname(host.toLatin1().constData());

    if(!hostEnt)
    {
        qWarning() << "Unable to collect gethostbyname\n";
    }

    m_server_info.sin_family = PF_INET;
    m_server_info.sin_addr = *((LPIN_ADDR)*hostEnt->h_addr_list);
    m_server_info.sin_port = htons(port);

    //connect(SOCKET s,const struct sockaddr *name,int namelen);
    int connect_ret=connect(m_socket,
                            (sockaddr *)&m_server_info,
                            sizeof(m_server_info));
    if(connect_ret==SOCKET_ERROR)
    {
        qWarning() << "Unable to connect\n";
    }
    else
    {
        m_isConnected = true;
    }
    return m_isConnected;
}

Command *ClientSocket::read()
{
    Command* command = nullptr;
    const int command_size = 4*2+2;// 2* int32 + 2*byte
    char buff[command_size];
    //qDebug() << "read";
    if (!m_isConnected)
        return command;

    int reading_byte = recv(m_socket, &buff[0], command_size, 0);

    if (reading_byte>0)
    {
        //qDebug() << "----> = " << reading_byte;
        quint8 size = buff[1];
        quint32 x,y;
        x = *((quint32*)&buff[2]);
        y = *((quint32*)&buff[6]);
//        for (int i =0; i<10; ++i){
//            int k = buff[i];
//            qDebug() << buff[i] << "  " <<  k;
//        }

        //qDebug() << "   size" << ((quint32)size) << " " << x << ", " << y;
        switch (buff[0]) {
        case Command::Pen:{
            command = new PenCommand(x,y,size);
            break;
        }
        case Command::Eraser:{
            command = new EraserCommand(x,y,size);
            break;
        }
        default:
            command = new Command();
            break;
        }
    }
    else
    {
        m_isConnected = false;
    }
    return command;
}

void ClientSocket::write(Command *command)
{
    quint8 *buff = command->data();
    send(m_socket, (char*)buff, command->dataSize(), 0);
}

bool ClientSocket::isConnected() const
{
    return m_isConnected;
}

ClientSocket::ClientSocket()
{
    m_isConnected = false;
}

bool Net::connect(QString host, unsigned int port){
    ClientSocket * network = ClientSocket::instance();
    return network->reConnect(host, port);
}

QList<Command *> Net::get(){
    std::lock_guard<std::mutex> lock_guard(m_guard_queue);
    QList<Command*> cmds(m_queue);
    m_queue.clear();
    return cmds;
}

void Net::send(Command *cmd){
    ClientSocket * network = ClientSocket::instance();
    network->write(cmd);
}

void Net::_thread(Net *t)
{
    ClientSocket * network;
    Command * cmd;
    while (true)
    {
        network = ClientSocket::instance();
        if (!network->isConnected()){
            std::this_thread::sleep_for(
                        std::chrono::seconds(1));
            continue;
        }
        cmd = network->read();
        if (cmd !=nullptr){
            t->m_guard_queue.lock();
            t->m_queue.append(cmd);
            t->m_guard_queue.unlock();
        }
    }
}

