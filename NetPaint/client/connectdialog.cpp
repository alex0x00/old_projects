#include "connectdialog.h"
#include "ui_connectdialog.h"

ConnectDialog::ConnectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectDialog)
{
    ui->setupUi(this);
}

ConnectDialog::~ConnectDialog()
{
    delete ui;
}

quint32 ConnectDialog::port()
{
    return ui->spinBox->value();
}

QString ConnectDialog::host()
{
    return ui->lineEdit->text();
}


