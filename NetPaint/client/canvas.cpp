#include "canvas.h"
#include <QPainter>
#include <QPen>
#include <QDebug>


Canvas::Canvas(QWidget *parent) :
    QWidget(parent)
{
    setAttribute(Qt::WA_StaticContents);//для оптимизации отрисовки
    m_modified = false;
    m_isDrawing = false;
    //_penColor = Qt::black;
    m_image = QImage(800, 600, QImage::Format_RGB32);
    m_image.fill(QColor(Qt::white));
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_lastPoint = event->pos();
        m_isDrawing = true;
//        emit action(event->pos());
//        qDebug() << "click:" << QVector2D(event->pos()) << "\n";
    }
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && m_isDrawing)
    {
        //qDebug() << "Move:" << QVector2D(event->pos()) << "\n";
        emit action(event->pos());
    }
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && m_isDrawing) {
        m_isDrawing = false;
    }
}

void Canvas::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, m_image, dirtyRect);
}

void Canvas::executeCommandDrawing(Command* command)
{
    if (command == nullptr)
        return;

    m_mutex.lock();
    QPainter painter(&m_image);
    QPoint endPoint;
    switch (command->type()) {
        case Command::Pen:{
            PenCommand penCommand = *(static_cast<PenCommand*>(command));
            QPen pen;
            pen.setWidth(penCommand.size()/2);
            painter.setPen(pen);
            endPoint = QPoint(penCommand.x(), penCommand.y());

            painter.drawPoint(endPoint);
            int rad = (penCommand.size() / 2);
            update(QRect(m_lastPoint, endPoint).normalized()
                   .adjusted(-rad, -rad, +rad, +rad));
            m_lastPoint = endPoint;
            break;
        }
        case Command::Eraser:{
            EraserCommand eraserCommand = *(static_cast<EraserCommand*>(command));
            endPoint = QPoint(eraserCommand.x(), eraserCommand.y());

            painter.eraseRect(eraserCommand.x(), eraserCommand.y(),
                               eraserCommand.size(), eraserCommand.size());
            int rad = (eraserCommand.size()/2);
            update(QRect(m_lastPoint, endPoint).normalized()
                   .adjusted(-rad, -rad, +rad, +rad));
            m_lastPoint = endPoint;

            break;
        }
    }
    m_mutex.unlock();
}

