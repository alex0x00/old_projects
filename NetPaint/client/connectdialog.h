#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include "network.h"
#include <QDesktopWidget>
#include <QDialog>

namespace Ui {
class ConnectDialog;
}

class ConnectDialog : public QDialog //класс формы запроса данных о подключеннии
{
    Q_OBJECT

public:
    explicit ConnectDialog(QWidget* parent = 0);
    ~ConnectDialog();
    quint32 port();
    QString host();
    Net& net()
    {
        return m_network;
    }
    void connect()
    {
        m_network.connect(host(), port());
    }

public slots:
    int exec()
    {
        QRect rect = frameGeometry();
        rect.moveCenter(QDesktopWidget().availableGeometry().center());
        move(rect.topLeft());
        return QDialog::exec();
    }

private:
    Ui::ConnectDialog* ui;
    Net m_network; // текущие соединение
};

#endif // CONNECTDIALOG_H
