#ifndef TOOLDRAW_H
#define TOOLDRAW_H

#include "command.h"
#include <QWidget>

namespace Ui {
class ToolDraw;
}

class ToolDraw : public QWidget // панель инструментов пользователя
{
    Q_OBJECT

public:
    explicit ToolDraw(QWidget* parent = 0);

    ~ToolDraw();
    Command::CommandType currentTool()
    {
        return m_type;
    }
    quint32 toolSize();

    void setEnabled(bool arg)
    {
        m_type = Command::None;
        QWidget::setEnabled(arg);
    }

private slots:
    void on_toolButtonPen_clicked(bool checked);

    void on_toolButtonEraser_clicked(bool checked);

private:
    Ui::ToolDraw* ui;
    Command::CommandType m_type;
};

#endif // TOOLDRAW_H
