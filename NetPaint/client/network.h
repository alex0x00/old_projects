#ifndef NETWORK_H
#define NETWORK_H
#include "command.h"

#include <QByteArray>
#include <QDebug>
#include <QList>
#include <QString>

#include <condition_variable>
#include <mutex>
#include <thread>
#include <winsock2.h>

class ClientSocket // класс клиента сервера
{
public:
    static ClientSocket* instance(void);
    ~ClientSocket();
    bool reConnect(QString host, unsigned int port); //метод установления подключения
    Command* read(void); //чтение команды
    void write(Command* command); //запись команды
    bool isConnected() const; //состояние подключения
    void close()
    {
        m_isConnected = false;
        closesocket(m_socket);
    }

private:
    ClientSocket();
    SOCKET m_socket; //клиентский сокет
    SOCKADDR_IN m_server_info; //данные о подключении к серверу
    bool m_isConnected; //TO-DO нужно защитить!
};

class Net {
public:
    Net()
    {
        m_thread = std::thread(&Net::_thread, this);
        //запуск потока работы с сетью чтобы не блокировать GUI на ожиданиях получения данных
    }
    bool connect(QString host, unsigned int port);
    QList<Command*> get(); // метод возращает накопленные комманды
    void send(Command* cmd); //отправляет команду серверу
    static void _thread(Net* t); //метод работающий в потоке
    bool isConnected() const
    {
        return ClientSocket::instance()->isConnected();
    }
    ~Net()
    {
        ClientSocket::instance()->close();
        if (m_thread.joinable()) {
            m_thread.join();
        }
    }

private:
    std::thread m_thread; // объект потока
    std::mutex m_guard_queue; //мьютекс защищает  m_queue
    QList<Command*> m_queue; //очередь накопленных команд
};

#endif // NETWORK_H
