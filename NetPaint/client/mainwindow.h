#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "connectdialog.h"
#include "network.h"
#include <QDesktopWidget>
#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow //класс формирует связь между объектами программы и элементами формы программы
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

public slots:
    void onActioon(const QPoint);
    void show()
    {
        QRect rect = frameGeometry();
        rect.moveCenter(QDesktopWidget().availableGeometry().center());
        move(rect.topLeft());
        QMainWindow::show();
    }

private slots:
    void on_action_3_triggered();
    void on_action_2_triggered();

    void on_action_triggered();
    void updateNetworkData(); //метод обработки накопленных команд для отрисовки
    void on_action_4_triggered();

private:
    Ui::MainWindow* ui;
    ConnectDialog* m_dialog;
    QTimer* m_timer;
    ClientSocket* m_clientSocket;
};

#endif // MAINWINDOW_H
