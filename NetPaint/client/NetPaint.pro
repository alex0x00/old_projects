#-------------------------------------------------
#
# Project created by QtCreator 2014-10-12T12:15:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NetPaint
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -lws2_32
win32:LIBS += -lsetupapi \
                        -lws2_32

SOURCES += main.cpp\
        mainwindow.cpp \
    connectdialog.cpp \
    canvas.cpp \
    tooldraw.cpp \
    command.cpp \
    network.cpp

HEADERS  += mainwindow.h \
    connectdialog.h \
    canvas.h \
    tooldraw.h \
    command.h \
    network.h

FORMS    += mainwindow.ui \
    connectdialog.ui \
    tooldraw.ui
