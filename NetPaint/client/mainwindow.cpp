#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "command.h"
#include <QDebug>
#include <QFileDialog>
#include <QList>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->widgetTool->setEnabled(false);
    connect(ui->widgetCanvas, SIGNAL(action(QPoint)),
            this, SLOT(onActioon(QPoint)));
    m_dialog = new ConnectDialog(this);
    m_timer = new QTimer(this);
    m_clientSocket = ClientSocket::instance();
    connect(m_timer, SIGNAL(timeout()),
            this, SLOT(updateNetworkData()));
    m_timer->setInterval(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onActioon(const QPoint pos){
    switch (ui->widgetTool->currentTool()) {
        case Command::Pen:{
            PenCommand penCommand(pos.x(), pos.y(),
                                  ui->widgetTool->toolSize());
            ui->widgetCanvas->executeCommandDrawing(&penCommand);
            m_dialog->net().send(&penCommand);
            //qDebug() << "Pen" ;
            break;
        }
        case Command::Eraser:{
            EraserCommand eraserCommand(pos.x(), pos.y(),
                                        ui->widgetTool->toolSize());
            ui->widgetCanvas->executeCommandDrawing(&eraserCommand);
            //qDebug() << "Eraser" ;
            m_dialog->net().send(&eraserCommand);
            break;
        }
    }
}

void MainWindow::on_action_3_triggered()
{
    m_dialog->setModal(true);
    int t = m_dialog->exec();
    m_dialog->connect();
    qDebug() << m_dialog->result();

    if (t==QDialog::Accepted)
    {
        if(m_dialog->net().isConnected())
        {
            ui->widgetTool->setEnabled(true);
            QMessageBox::information(this, QString("Connect status"),
            QString("Create connect to server:\n  potr %1\n  host: %2").arg(m_dialog->port()).arg(m_dialog->host()));
            qDebug() << "start timer";
            m_timer->start();
        }
        else
        {
            ui->widgetTool->setEnabled(false);
            QMessageBox::warning(this,
                                  QString("Connect status"),
            QString("Error: not connect to server:\n  potr:%1\n  host:%2").arg(m_dialog->port()).arg(m_dialog->host()));

        }
    }

}

void MainWindow::on_action_2_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                "./",
                                                tr("Images (*.bmp *.jpg *.jpeg *.png *.ppm *.xbm *xpm)"));
    if (!path.isEmpty()){
    ui->widgetCanvas->image().save(path);
    }
}

void MainWindow::on_action_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                "./",
                                                tr("Images (*.bmp *.gif  *.jpg *.jpeg *.png *.pbm *.pgm *.ppm *.xbm *xpm)"));
    if (!path.isEmpty()){
    QImage image(path);
    ui->widgetCanvas->setImage(image);
    }
}

void MainWindow::updateNetworkData()
{
    QList<Command*> cmds = m_dialog->net().get();

    foreach (Command* cmd, cmds) {
        ui->widgetCanvas->executeCommandDrawing(cmd);
        delete cmd;
    }
    if (!m_dialog->net().isConnected()){
        m_timer->stop();
        QMessageBox::information(this, QString("Connect status"),
        QString("Disconnect to server:\n  potr %1\n  host: %2").arg(m_dialog->port()).arg(m_dialog->host()));
        qDebug() << "stop timer";
        ui->widgetTool->setEnabled(false);
    }
}


void MainWindow::on_action_4_triggered()
{
    QApplication::exit();
}

