#ifndef CANVAS_H
#define CANVAS_H

#include "command.h"
#include <QDebug>
#include <QList>
#include <QMouseEvent>
#include <QMutex>
#include <QPainter>
#include <QPen>
#include <QVector2D>
#include <QWidget>

class Canvas : public QWidget {
    Q_OBJECT
public:
    explicit Canvas(QWidget* parent = 0);

signals:
    void action(const QPoint);

protected:
    void mousePressEvent(QMouseEvent* event); //обработчик нажатия
    void mouseMoveEvent(QMouseEvent* event); // обработчик пермещения
    void mouseReleaseEvent(QMouseEvent* event); // обработка при отпускании
    void paintEvent(QPaintEvent* event); //обработка события перерисовки изображения

public slots:
    void executeCommandDrawing(Command* command); //метод отрисовки команды (вызывается из потока)
    QImage image() //возвращает текущие изображение
    {
        return m_image;
    }
    void setImage(QImage& image) // загружает изображение
    {
        m_image = image;
    }

private:
    //    QColor  _penColor;
    QImage m_image; //объект изображения
    QPoint m_lastPoint;
    bool m_isDrawing;
    bool m_modified;
    QMutex m_mutex; //защиаем данные
};

#endif // CANVAS_H
