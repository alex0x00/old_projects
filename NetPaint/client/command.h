#ifndef COMMAND_H
#define COMMAND_H

#include <QByteArray>
#include <QDebug>
#include <QObject>

//классы команд отрисовки
class Command {
public:
    enum CommandType {
        None = 0,
        Pen,
        Eraser
    };
    Command(quint8 type = None)
        : m_type(type)
    {
    }
    ~Command()
    {
    }

    virtual quint8* data()
    {
        quint8* ret = new quint8;
        *ret = m_type;
        //return QByteArray((char *)ret, 1);
        return ret;
    }

    virtual quint8 type()
    {
        return m_type;
    }

    virtual quint32 dataSize()
    {
        return sizeof(m_type);
    }
    static bool pack(quint8* array, //массив результата
        quint32 offset, //смещение в этом массиве
        quint32 array_length, //длина массива (контроль переполнения)
        quint8* data, //данные
        quint32 data_size) //размер данных (тип)
    { //упаковка типа в массив
        if (offset + data_size > array_length) {
            Q_ASSERT(true);
            return false;
        }
        //qDebug() << "pack";
        for (quint32 i = 0; i < data_size; ++i) {
            //int k = data[i];

            array[offset + i] = data[i];
            //qDebug() << (int)array[offset + i] << "  "  << (int)data[i] << "  ="<<  k;
        }
        return true;
    }
    /*

     *       pack(raw_data,
             1 + sizeof(m_size),
             dataSize(),
             (quint8*)&m_x,
             sizeof(m_x));
     * */

private:
    quint8 m_type;
};

class PenCommand : public Command {
public:
    PenCommand(qint32 x, qint32 y, quint8 size)
        : Command(Pen)
        , m_x(x)
        , m_y(y)
        , m_size(size)
    {
    }
    quint32 dataSize()
    {
        return Command::dataSize()
            + sizeof(m_x)
            + sizeof(m_y)
            + sizeof(m_size);
    }
    quint8* data()
    {
        quint8* raw_data = new quint8[dataSize()];
        QByteArray ret;
        for (int i = 0; i < 10; ++i) {
            raw_data[i] = 0;
        }
        raw_data[0] = type();
        //qDebug() << "data" << m_x << ", "<< m_y;

        pack(raw_data,
            1,
            dataSize(),
            (quint8*)&m_size,
            sizeof(m_size));
        //        qDebug() << "______data______1";
        //        for (int i =0; i<10; ++i){
        //            int k = raw_data[i];
        //            qDebug() << raw_data[i] << "  " <<  k;
        //        }

        pack(raw_data,
            2,
            dataSize(),
            (quint8*)&m_x,
            sizeof(m_x));
        //        qDebug() << "______data______2";
        //        for (int i =0; i<10; ++i){
        //            int k = raw_data[i];
        //            qDebug() << raw_data[i] << "  " <<  k;
        //        }

        pack(raw_data,
            6,
            dataSize(),
            (quint8*)&m_y,
            sizeof(m_y));
        //ret.append((const char*)raw_data, dataSize());
        //        qDebug() << "______data______3";
        //        for (int i =0; i<10; ++i){
        //            int k = raw_data[i];
        //            qDebug() << raw_data[i] << "  " <<  k;
        //        }

        return raw_data;
    }
    quint32 x()
    {
        return m_x;
    }
    quint32 y()
    {
        return m_y;
    }

    quint16 size()
    {
        return m_size;
    }

private:
    quint32 m_x, m_y;
    quint8 m_size;
};

class EraserCommand : public Command {
public:
    EraserCommand(qint32 x, qint32 y, quint8 size)
        : Command(Eraser)
        , m_x(x)
        , m_y(y)
        , m_size(size)
    {
    }
    quint32 dataSize()
    {
        return sizeof(EraserCommand);
    }
    quint8* data()
    {
        quint8* raw_data = new quint8[dataSize()];
        QByteArray ret;
        raw_data[0] = type();

        pack(raw_data,
            1,
            dataSize(),
            (quint8*)&m_size,
            sizeof(m_size));

        pack(raw_data,
            2,
            dataSize(),
            (quint8*)&m_x,
            sizeof(m_x));

        pack(raw_data,
            6,
            dataSize(),
            (quint8*)&m_y,
            sizeof(m_y));
        //ret.append((const char*)raw_data, dataSize());

        return raw_data;
    }
    quint32 x()
    {
        return m_x;
    }
    quint32 y()
    {
        return m_y;
    }
    quint16 size()
    {
        return m_size;
    }

private:
    quint32 m_x, m_y;
    quint8 m_size;
};

#endif // COMMAND_H
