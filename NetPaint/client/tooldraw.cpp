#include "tooldraw.h"
#include "ui_tooldraw.h"

ToolDraw::ToolDraw(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToolDraw)
{
    ui->setupUi(this);
}

ToolDraw::~ToolDraw()
{
    delete ui;
}

quint32 ToolDraw::toolSize()
{
    return ui->spinBoxSize->value();
}

void ToolDraw::on_toolButtonPen_clicked(bool checked)
{
    if (checked){
        m_type = Command::Pen;
        ui->toolButtonEraser->setChecked(false);
    }
    else
        m_type = Command::None;
}

void ToolDraw::on_toolButtonEraser_clicked(bool checked)
{
    if (checked){
        m_type = Command::Eraser;
    ui->toolButtonPen->setChecked(false);
    }
    else
        m_type = Command::None;
}
