#include "client.h"
#include "store.h"
#define BUFFER_FOR_READ 10
Client::Client(Store* store, SOCKET client_socket)
{
    m_store = store;
    m_client = client_socket;
    m_isRun = false;
}

Client::~Client()
{
    m_guard_write.lock();
    std::for_each(m_write.begin(), m_write.end(), [](Buffer* buff) { delete buff; });
    m_write.clear();
    m_guard_write.unlock();
    closesocket(m_client);
    m_guard_data_self.lock();
    m_isRun = false;
    m_guard_data_self.unlock();

    if (m_thread.joinable()) {
        m_thread.join();
    }
}

void Client::addMessage(Buffer* buf)
{
    //std::cout << "addMessage" <<std::endl;
    m_guard_write.lock();
    m_write.push_back(buf);
    //std::cout <<"+list " << m_write.size() << std::endl;

    m_guard_write.unlock();
}

void Client::sendMessage(Buffer* buff)
{ //отправлем сообщение клиенту (пишем в сокет )
    send(this->m_client, buff->data(), buff->length(), 0); //отправляем данные и чистим память
    delete buff;
}

bool Client::isRun()
{
    bool ret;
    m_guard_data_self.lock();
    ret = m_isRun;
    m_guard_data_self.unlock();
    return ret;
}

void Client::start()
{
    m_guard_data_self.lock();
    bool run = m_isRun;
    m_guard_data_self.unlock();
    if (!run) {
        m_isRun = true;
        m_thread = std::thread(&Client::clientThread, this);
    } else {
        std::cout << "error double call start" << std::endl;
    }
}

void Client::write()
{ //отправлем все сообщения клиенту (пишем в сокет )
    m_guard_write.lock();
    //    if (!m_write.empty()){
    //        std::cout << "write \n send :" << m_client << "list " << m_write.size() << std::endl;
    //    }

    for (auto i = m_write.begin(); i != m_write.end(); ++i) {
        Buffer* buff = *i;
        send(this->m_client, buff->data(), buff->length(), 0); //отправляем данные и чистим память
        delete buff;
    }
    m_write.clear(); //все отправили, чистим
    m_guard_write.unlock();
}

void Client::read()
{
    char buff[BUFFER_FOR_READ];
    int reading_byte;
    reading_byte = recv(m_client, buff, BUFFER_FOR_READ, 0);
    //std::cout << "read \n client :" << m_client << "reading  buff:" << buff << std::endl;
    if (reading_byte == SOCKET_ERROR) {
        //std::cout << "SOCKET_ERROR error read socket" << std::endl;
        m_guard_data_self.lock();
        m_isRun = false;
        m_guard_data_self.unlock();
    } else {
        Buffer* buffer = new Buffer(&buff[0], reading_byte);
        m_store->sendAllMessage(this, buffer);
        delete buffer;
    }
}

void Client::clientThread(Client* t)
{
    bool run = true;
    while (run) {
        t->read(); //t->write();
        t->m_guard_data_self.lock();
        run = t->m_isRun;
        t->m_guard_data_self.unlock();
    }
}
