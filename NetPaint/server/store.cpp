#include "store.h"

Store::Store()
{
}

void Store::addClient(SOCKET client_socket)
{
    Client* client = new Client(this, client_socket);
    m_guard_data_self.lock();
    m_clients.push_back(client);
    m_guard_data_self.unlock();
    client->start();
}

void Store::sendAllMessage(Client* const client_sender, Buffer* buff)
{
    m_guard_data_self.lock();
    //std::cout << "sendAllMessage " << m_clients.size() << std::endl;
    for (auto i = m_clients.begin(); i != m_clients.end(); ++i) {
        Client* client = *i;
        if (client != client_sender) {
            //std::cout << "sendAllMessage <<<<" << std::endl;
            Buffer* buffer = new Buffer(buff->data(), buff->length());
            client->sendMessage(buffer);
        }
    }
    m_guard_data_self.unlock();
}

Store::~Store()
{
    m_guard_data_self.lock();
    for (auto i = m_clients.begin(); i != m_clients.end(); ++i) {
        Client* client = *i;
        delete client;
    }
    m_clients.clear();
    m_guard_data_self.unlock();
}

int Store::count()
{
    m_guard_data_self.lock();
    int ret = m_clients.size();
    m_guard_data_self.unlock();
    return ret;
}
