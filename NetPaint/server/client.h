#ifndef CLIENT_H
#define CLIENT_H
#include <mutex>
#include <list>
#include <iostream>
#include <algorithm>
#include <winsock2.h>
#include <thread>

#include "buffer.h"


class Store;

class Client{
    //класс работает с одним клиентом в потоке.
    //При получении сообщений от других клиентов накапливает и при отправке отправляет все сообщения
    //При получении данных от своего клиента добавляет в хранилище своё сообщение для оповещения других клиентов

public:
    Client(Store* store, SOCKET client_socket);//конструктор объекта с параметрами
    ~Client();
    void addMessage(Buffer* buf);//добавление сообщения в накопитель m_write вызывается друким потоком
    void sendMessage(Buffer* buff);
    bool isRun();// метод опроса состояния

    void start();//запуск потока обработки клиента
private:
    Store* m_store;//хранилеще клиентов
    SOCKET m_client;//сокет клиента
    std::list<Buffer*> m_write;//что нам нужно отправить
    std::mutex m_guard_write;// объект примитива синхронизации для сообщений (защищает m_write)
    std::thread m_thread;//объект потока обработки
    std::mutex m_guard_data_self;//объект примитива синхронизации данных класса (защищает m_isRun)
    bool m_isRun;

    void write();//запись накопленных данных
    void read();//чтение данных с сокета
    static void clientThread(Client*);//метод работает в потоке чтобы не блокировать программу ожиданием поступлением данных

};
#endif // CLIENT_H
