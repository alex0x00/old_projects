#ifndef BUFFER_H
#define BUFFER_H

class Buffer { //класс байтового буфера (назначение хранение сообщений как есть)
public:
    Buffer();
    Buffer(char* data, unsigned int length);
    Buffer& operator=(Buffer const& other);
    ~Buffer();
    char* data() const;
    unsigned int length() const;

private:
    char* m_data;
    unsigned int m_length;
};

#endif // BUFFER_H
