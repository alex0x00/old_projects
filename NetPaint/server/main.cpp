#include "server.h"
#include <iostream>
//#pragma comment(lib, "wsock32.lib")
#define PORT 8888
#define MAX_ACTIVE_CLIENTS 2

int main()
{
    int menu_mode = 0;
    int port = PORT, clients = MAX_ACTIVE_CLIENTS;
    do {
        std::cout << "Create server.\n Input port:" << std::endl;
        std::cin >> port;
        std::cin.clear();
        std::cin.sync();
    } while (!(port > 0 && port < 65535));

    do {
        std::cout << "\nInput count clients:" << std::endl;
        std::cin >> clients;
        std::cin.clear();
        std::cin.sync();
    } while (!(clients > 0 && clients < 65535));

    Server server(port, clients, 1000);
    server.start();
    do {
        std::cout << "Input command:" << std::endl
                  << "\texit - 1" << std::endl
                  << "\tstatus - 2" << std::endl;
        std::cin >> menu_mode;
        std::cin.clear();
        std::cin.sync();
        switch (menu_mode) {
        case 1:
            server.stop();
            exit(0);
            break;
        case 2: //status
            std::cout << std::boolalpha << "status:" << std::endl
                      << "\trun - " << server.isRun() << std::endl
                      << "\tclients - " << server.clients() << std::endl;
            break;
        default:
            break;
        }

    } while (menu_mode != 1);

    return 0;
}
