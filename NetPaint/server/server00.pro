TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    buffer.cpp \
    store.cpp \
    client.cpp \
    server.cpp

QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -lws2_32
win32:LIBS += -lsetupapi \
                        -lws2_32

HEADERS += \
    buffer.h \
    store.h \
    client.h \
    server.h

