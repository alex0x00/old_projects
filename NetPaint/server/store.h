#ifndef STORE_H
#define STORE_H
#include <mutex>
#include <list>
#include <algorithm>
#include <winsock2.h>
#include "client.h"

class Store
{
public:
    Store();
    void addClient(SOCKET client_socket);//добавляет клиента в список
    void sendAllMessage(Client * const client_sender, Buffer* buff);// отправляет всем клиентам сообщение
    ~Store();
    int count();//возращает колличество клиентов
private:
    std::mutex m_guard_data_self;//объект синхронизации данных класса (защищает m_clients)
    std::list<Client*> m_clients;//список клиентов
};
#endif // STORE_H
