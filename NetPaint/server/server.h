#ifndef SERVER_H
#define SERVER_H
#include "store.h"
#include <iostream>
#include <mutex>
#include <thread>
#include <winsock2.h>

class Server {
public:
    Server(int port, int clients, int timeout);
    void start();
    bool isRun();
    void stop();
    int clients();
    int countConnects();
    ~Server();

private:
    static bool init();
    static void acceptClientThread(Server* t);
    SOCKET m_server; //сокет сервера
    int m_count_connects; //колличество клиентов
    int m_port; //порт сервера
    int m_timeout; //TO-DO время отключения от клиента
    bool m_isRun; // состояние сервера
    Store* m_clients; //клиента
    std::thread m_thread; //объект потока
    std::mutex m_guard_confing; //защита данных класса
};

#endif // SERVER_H
