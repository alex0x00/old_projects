#include "buffer.h"
#include <algorithm>

Buffer::Buffer()
{
    m_length = 0;
    m_data = nullptr;
}

Buffer::Buffer(char* data, unsigned int length)
{
    m_length = length;
    m_data = new char[length];
    for (int i = 0; i < length; ++i)
        m_data[i] = data[i];
}

Buffer& Buffer::operator=(const Buffer& other)
{
    if (this != &other) // защита от неправильного самоприсваивания
    {
        // освобождаем "старую" память
        delete[] m_data;
        // выделяем "новую" память и копируем элементы
        m_data = new char[other.m_length];
        std::copy(other.m_data, other.m_data + other.m_length, m_data);
        m_length = other.m_length;
    }
    // по соглашению всегда возвращаем *this
    return *this;
}

Buffer::~Buffer()
{
    if (m_data != nullptr)
        delete[] m_data;
}

char* Buffer::data() const
{
    return m_data;
}

unsigned int Buffer::length() const
{
    return m_length;
}
