#include "server.h"

Server::Server()
{
}

Server::Server(int port, int clients, int timeout) //конструктор с параметрами (TO-DO timeout - таймаут отключения от клиента)
    : m_port(port),
      m_count_connects(clients),
      m_timeout(timeout),
      m_isRun(false),
      m_clients(new Store)
{
}

void Server::start()
{ // запуск сервера
    sockaddr_in local;
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = INADDR_ANY;
    local.sin_port = htons((u_short)m_port);
    init();
    m_server = socket(AF_INET, SOCK_STREAM, 0); //получаем дескриптор сокета сервера
    if (m_server == INVALID_SOCKET) {
        std::cout << "Error : INVALID_SOCKET!" << std::endl;
        return;
    }
    if (bind(m_server, (sockaddr*)&local, sizeof(local)) != 0) {
        std::cout << "Error : bind!" << std::endl;
        return;
    }
    if (listen(m_server, m_count_connects) != 0) //слушаем сокет
    {
        std::cout << "Error : listen!" << std::endl;
        return;
    }
    m_isRun = true;
    if (m_clients != nullptr) {
        m_clients = new Store;
    }
    m_thread = std::thread(&Server::acceptClientThread, this); //зпускаем нить для обработки заявок на подключение
    //m_thread.detach();
}

bool Server::isRun() //статус сервера
{
    bool ret;
    m_guard_confing.lock();
    ret = m_isRun;
    m_guard_confing.unlock();
    return ret;
}

void Server::stop()
{ //останавливает сервер
    m_guard_confing.lock();
    m_isRun = false;
    m_guard_confing.unlock();
    closesocket(m_server);
    if (m_thread.joinable()) {
        m_thread.join(); //(нужно аварийно завершать поток)
    }
    delete m_clients;
}

int Server::clients()
{
    return m_clients->count();
}

int Server::countConnects()
{
    return m_count_connects;
}

Server::~Server()
{
    stop();
}

bool Server::init() //метод инициализации библиотеки
{
    WSADATA wsa_data;
    static bool wsa_init = false;
    int wsa_ret;
    if (!wsa_init) {
        wsa_ret = WSAStartup(MAKEWORD(2, 2), &wsa_data); //mssock v2.2
        if (wsa_init != 0) {
            return false;
        } else {
            wsa_init = true;
        }
    }
    return true;
}

void Server::acceptClientThread(Server* t) //метод обрабатывает запросы на подключение
{
    SOCKET client;
    sockaddr_in from;
    int fromlen = sizeof(from);
    bool run = true;
    while (run) {
        if (t->clients() < t->countConnects()) {
            client = accept(t->m_server,
                (struct sockaddr*)&from, &fromlen);
            t->m_clients->addClient(client);
        }
        t->m_guard_confing.lock();
        run = t->m_isRun;
        t->m_guard_confing.unlock();
    }
}
