#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "scene.h"
#include <QOpenGLWidget>
#include <QtWidgets>

class AnimationViewer : public QOpenGLWidget {
    Q_OBJECT

public:
    explicit AnimationViewer(QWidget* parent = 0);
    ~AnimationViewer();
    void keyPressEvent(QKeyEvent* event); //обработчик нажатий
protected:
    void initializeGL(); //инициализация opengl
    void paintGL();
    void resizeGL(int width, int height);

private:
    float m_aspectRatio; //отношение ширины и высоты
    QVector3D m_position; //позиция камеры
    QVector3D m_center; //точка взгляда
    QTimer* m_timer; //таймер отрисовки
    QMatrix4x4 m_projection; //матрица проекции
    QMatrix4x4 m_view; //матрица точки обзора
    Book m_book;
};

#endif
