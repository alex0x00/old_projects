#include "AnimationViewer.h"
#include <QApplication>
#include <QtCore>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    AnimationViewer window;

    window.show();

    return app.exec();
}
