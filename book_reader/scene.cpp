#include "scene.h"
#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QGLWidget>
#include <QImage>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QVector>
#include <QtOpenGL>
#include <qopengl.h>
//TO-DO заменить пересоздание текстур обновлением
GLuint createTexture(QImage& image)
{
    //создаем текстуру с нашим изображением
    QImage prepare;
    //преобразуем изображение в формат для opengl
    prepare = QGLWidget::convertToGLFormat(image);
    GLuint texture;
    glGenTextures(1, &texture); //запрашиваем индекс текстуры
    //используем этот текстурный
    //индекс  как текущий
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //передаем данные
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
        prepare.width(), prepare.height(),
        0, GL_RGBA, GL_UNSIGNED_BYTE, prepare.bits());
    //параметры фильтрации
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //параметры наложения текстуры растяжение или повторы
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    return texture;
}

void updateTexture(QImage& image, GLuint texture)
{
    //создаем текстуру с нашим изображением
    QImage prepare;
    //преобразуем изображение в формат для opengl
    prepare = QGLWidget::convertToGLFormat(image);
    //используем этот текстурный
    //индекс  как текущий
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //передаем данные
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
        prepare.width(), prepare.height(),
        0, GL_RGBA, GL_UNSIGNED_BYTE, prepare.bits());
    //параметры фильтрации
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    //параметры наложения текстуры растяжение или повторы
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}

Book::Book(const QSize& page)
{
    m_page_size = page;
    m_animate_state = AnimateState::NONE;
    m_state = BookState::BEGIN;
}

Book::~Book()
{
    for (unsigned int i = 0; i < VIEW_PAGE; ++i) {
        glDeleteTextures(1, &m_texture[i]);
    }
}

bool Book::open()
{
    m_animate_state = AnimateState::NONE;
    m_state = BookState::BEGIN;
    if (m_spread_right.isEmpty()) {
        return false;
    }
    m_texture[VIEW_RIGHT] = createTexture(m_spread_right.top().left);
    return true;
}

bool Book::next()
{ //листаем влево и переходим на следующую страницу
    if (isAnimate() || m_state == BookState::END) {
        return false;
    }
    if (m_state == BookState::BEGIN) {
        m_state = BookState::NORMAL;
    }

    m_animate_state = AnimateState::MOVE_NEXT;
    m_currnet = m_spread_right.pop();

    m_texture[1] = createTexture(m_currnet.left);
    m_texture[2] = createTexture(m_currnet.right);

    if (m_spread_right.isEmpty()) {
        m_state = BookState::END;
    } else {
        m_texture[VIEW_RIGHT] = createTexture(m_spread_right.top().left);
    }
    if (!m_spread_left.isEmpty()) {
        m_texture[VIEW_LEFT] = createTexture(m_spread_left.top().right);
    }
    m_animate = 0.0f;
}

bool Book::prev()
{ //листаем вправо и переходим на предыдущую страницу
    if (isAnimate() || m_state == BookState::BEGIN) {
        return false;
    }

    if (m_state == BookState::END) {
        m_state = BookState::NORMAL;
    }

    m_animate_state = AnimateState::MOVE_PREV;
    m_currnet = m_spread_left.pop();

    m_texture[1] = createTexture(m_currnet.left);
    m_texture[2] = createTexture(m_currnet.right);

    if (m_spread_right.isEmpty()) {
        m_state = BookState::END;
    } else {
        m_texture[VIEW_RIGHT] = createTexture(m_spread_right.top().left);
    }
    if (!m_spread_left.isEmpty()) {
        m_texture[VIEW_LEFT] = createTexture(m_spread_left.top().right);
    } else {
        m_state = BookState::BEGIN;
    }
    m_animate = 0.0f;
}

bool Book::load(const QString& src)
{ //путь к книге
    m_src = src;
    m_spread_right.clear();
    QFile loadFile(src);
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Error open file.");
        return false;
    }
    QByteArray data = loadFile.readAll();
    QJsonParseError parse_error;
    QJsonDocument json_doc(QJsonDocument::fromJson(data, &parse_error));
    QJsonObject json_obj = json_doc.object();
    if (!json_obj[JSON_CONTENT_PAGES].isArray()) {
        qDebug() << QString("Error parse file.") << parse_error.errorString();
        return false;
    }
    QJsonArray pages = json_obj[JSON_CONTENT_PAGES].toArray();
    QVector<QImage> image_pages;
    for (int i = 0; i < pages.count(); ++i) {
        if (pages[i].isString()) {
            qDebug() << pages[i];
            image_pages.push_back(QImage(pages[i].toString()));
        }
    }
    if (image_pages.length() % 2 != 0) {
        qWarning("Error load all page in file.");
        return false;
    }
    for (int i = 0; i < image_pages.length(); i += 2) {
        Page page(image_pages[i + 0], image_pages[i + 1]);
        m_spread_right.push_front(page);
    }
    return true;
}

void Book::draw()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_TEXTURE_2D);
    if (!m_spread_left.isEmpty()) { //левый разворот
        glRotatef(180.0, 0.0, 0.0, 1.0);
        glBindTexture(GL_TEXTURE_2D, m_texture[VIEW_LEFT]);
        drawGridPage();
    }
    if (!m_spread_right.isEmpty()) { //правый разворот
        glBindTexture(GL_TEXTURE_2D, m_texture[VIEW_RIGHT]);
        glLoadIdentity();
        drawGridPage();
    }
    if (!isAnimate())
        return;
    glLoadIdentity();
    if (m_animate_state == AnimateState::MOVE_NEXT) {
        if (m_animate < 0.5f) {
            glBindTexture(GL_TEXTURE_2D, m_texture[1]);
        } else {
            glBindTexture(GL_TEXTURE_2D, m_texture[2]);
        }
        glRotatef(180.0f * m_animate, 0.0, 0.0, 1.0);
    }
    if (m_animate_state == AnimateState::MOVE_PREV) {
        if (m_animate < 0.5f) {
            glBindTexture(GL_TEXTURE_2D, m_texture[2]);
        } else {
            glBindTexture(GL_TEXTURE_2D, m_texture[1]);
        }
        glRotatef(180.0f * (1.0f - m_animate), 0.0, 0.0, 1.0);
    }
    drawGridPage();
}

void Book::drawGridPage()
{
    //обход вершин
    //    0--1
    //    |  |
    //    3--2
    //на z-x плоскости

    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(0.0, 0.0, m_page_size.height() / 2);

    glTexCoord2f(1.0, 0.0);
    glVertex3f(m_page_size.width(), 0.0, m_page_size.height() / 2);

    glTexCoord2f(1.0, 1.0);
    glVertex3f(m_page_size.width(), 0.0, -m_page_size.height() / 2);

    glTexCoord2f(0.0, 1.0);
    glVertex3f(0.0, 0.0, -m_page_size.height() / 2);
    glEnd();
}

void Book::animate()
{
    if (isAnimate()) {
        m_animate += VELOCITY_MOVE_PAGE;
        if (m_animate > 1.0f) {
            if (m_animate_state == AnimateState::MOVE_PREV) {
                m_spread_right.push(m_currnet);
                m_texture[VIEW_RIGHT] = m_texture[1]; //createTexture(m_currnet.left);
            }
            if (m_animate_state == AnimateState::MOVE_NEXT) {
                m_spread_left.push(m_currnet);
                m_texture[VIEW_LEFT] = m_texture[2]; //createTexture(m_currnet.right);
            }
            m_animate_state = AnimateState::NONE;
        }
    }
}
