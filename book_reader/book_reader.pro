TEMPLATE = app
TARGET = book_reader
QT += opengl widgets
CONFIG += c++14
LIBS += -lopengl32
HEADERS = \
    scene.h \
    AnimationViewer.h
SOURCES = \
        main.cpp \
    scene.cpp \
    AnimationViewer.cpp

