#include "AnimationViewer.h"
#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QtOpenGL>

AnimationViewer::AnimationViewer(QWidget* parent)
    : QOpenGLWidget(parent)
    , m_position(QVector3D(0, 8, 0))
    , m_center(QVector3D(0, 0, 0))
    , m_book(QSize(3, 4))
{
    QSurfaceFormat fmt; //настройки формата opengl
    fmt.setSamples(4);
    setFormat(fmt);
    m_timer = new QTimer(this);
    QObject::connect(m_timer, SIGNAL(timeout()),
        this, SLOT(update()));
    m_timer->setInterval(10);
    m_timer->start();
}

AnimationViewer::~AnimationViewer()
{
}

void AnimationViewer::initializeGL()
{
    m_aspectRatio = width() / height(); //отношения сторон
    m_projection.perspective(30, m_aspectRatio, 1, 100); //настройка перспективы

    m_view.lookAt(m_position,
        m_center,
        QVector3D(0, 0, -1.0)); //настройка точки обзора

    QOpenGLContext::currentContext();
    //разрешаем тест глубины

    glEnable(GL_DEPTH_TEST);
    // устанавливаем параметры для того чтоб не было резких границ между прозрачным местом и текстурой:
    glAlphaFunc(GL_GREATER, 0.0);

    glEnable(GL_BLEND); //Включаем режим смешивания цветов
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //параметры смешивания
}

void AnimationViewer::paintGL()
{
    QOpenGLContext::currentContext();
    m_book.animate();
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f); //цвет фона
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //очищаем буферы
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    QMatrix4x4 vp;

    vp = m_projection * m_view; //формируем итогувую матрицу проекции

    glMatrixMode(GL_PROJECTION); //меняем тип текущей матрицы на проекционную
    glLoadMatrixf(vp.data()); //загрузка матрицы
    glMatrixMode(GL_MODELVIEW); //меняем тип текущей матрицы на модельную
    glLoadIdentity();
    m_book.draw();
}

void AnimationViewer::resizeGL(int width, int height) //обработчик изменения размера окна
{
    QOpenGLContext::currentContext();
    glViewport(0, 0, width, height); //обновлем порт вывода opengl
    m_aspectRatio = width / height;
    m_projection.setToIdentity();
    m_projection.perspective(60, m_aspectRatio, 1, 1000); //обновляем перспективу
    update(); //обновляем (перерисовываем )
}

void AnimationViewer::keyPressEvent(QKeyEvent* event) //обработчик нажатий клавиш
{
    float step = 1.0f;
    //выходи по escape
    //управляем положением камеры
    switch (event->key()) {
    case Qt::Key_Escape: {
        exit(0);
        break;
    }
    case Qt::Key_2: {
        m_position += QVector3D(0.0, 0.0, step);
        m_center += QVector3D(0.0, 0.0, step);
        break;
    }
    case Qt::Key_8: {
        m_position += QVector3D(0.0, 0.0, -step);
        m_center += QVector3D(0.0, 0.0, -step);
        break;
    }
    case Qt::Key_Plus: { //вверх
        m_position += QVector3D(0.0, step, 0.0);
        m_center += QVector3D(0.0, step, 0.0);
        break;
    }
    case Qt::Key_Minus: { //вниз
        m_position += QVector3D(0.0, -step, 0.0);
        m_center += QVector3D(0.0, -step, 0.0);
        break;
    }
    case Qt::Key_4: {
        m_position += QVector3D(-step, 0.0, 0.0);
        m_center += QVector3D(-step, 0.0, 0.0);
        break;
    }
    case Qt::Key_6: {
        m_position += QVector3D(step, 0.0, 0.0);
        m_center += QVector3D(step, 0.0, 0.0);
        break;
    }
    case Qt::Key_Right: {
        m_book.next();
        break;
    }
    case Qt::Key_Left: {
        m_book.prev();
        break;
    }
    case Qt::Key_F3: {
        QString fileName = QFileDialog::getOpenFileName(
            this,
            tr("Файл книги"),
            "./",
            tr("Text (*.json *.js)"));
        m_book.load(fileName);
        m_book.open();
        break;
    }
    }
    m_view.setToIdentity();
    //матрица обзора
    m_view.lookAt(m_position,
        m_center,
        QVector3D(0, 0, -1));
    update();
}
