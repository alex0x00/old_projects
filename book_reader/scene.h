#ifndef SCENE_H
#define SCENE_H
#include <QImage>
#include <QObject>
#include <QOpenGL.h>
#include <QSize>
#include <QStack>
#include <QTimer>


const unsigned int VIEW_PAGE = 4;
const unsigned int VIEW_LEFT = 0;
const unsigned int VIEW_RIGHT = 3;
const char JSON_CONTENT_PAGES[] = "pages";
const float VELOCITY_MOVE_PAGE = 0.01f;

GLuint createTexture(QImage& image); //создаёт рессурс текстуры
void updateTexture(QImage& image, GLuint texture); // обновляем текстурные данные
struct Page {
public:
    Page(const QImage& l, const QImage& r)
        : left(l)
        , right(r)
    {
    }
    Page()
    {
    }
    QImage left;
    QImage right;
};

class Book { //класс книги
public:
    enum class BookState { BEGIN,
        NORMAL,
        END }; //состояния книги
    enum class AnimateState { MOVE_NEXT,
        MOVE_PREV,
        NONE }; //состояния анимации
    Book(const QSize& page); //разрешение страницы
    ~Book();

    bool open();

    bool next();

    bool prev();

    bool load(const QString& src);

    bool isAnimate()
    {
        return m_animate_state != AnimateState::NONE;
    }
    void animate();

    void draw();

    void drawGridPage();

private:
    QString m_src;
    QSize m_page_size;
    float m_animate; //[0.0, 1.0]
    GLuint m_texture[VIEW_PAGE];
    QStack<Page> m_spread_left;
    QStack<Page> m_spread_right;
    AnimateState m_animate_state;
    BookState m_state;
    Page m_currnet;
};

#endif // SCENE_H
