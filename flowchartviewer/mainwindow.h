#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "glwidget.h"
#include "controlform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void loadFileAlgoritm(const QString& src);
    void saveFileAlgoritm(const QString& src);
    QVector<FlowchartShape*>* flowchartItems();
    void clearFlowchartItems();
    void keyPressEvent(QKeyEvent* event);
    ~MainWindow();

public slots://специальное обазначение для обработчиков
    //slots для соединения connect
    void openDialogFileLoad();//открывает диалог с выбором файла
    void openDialogFileSave();
    void exit();
    void clearWorkSpace();
    void showControl();
    void updateGLWidget();
    void updateControlInformation();

private:
    Ui::MainWindow *ui;
    QString m_path;
    ControlForm* m_controlForm;
};

#endif // MAINWINDOW_H
