#include "controlform.h"
#include "ui_controlform.h"

ControlForm::ControlForm(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::ControlForm)
{
    ui->setupUi(this);
    m_flowchartItems = 0;
    //чтобы не редактировать елементы списка
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //обработчик при выделении элемента
    connect(ui->listView,
            SIGNAL(clicked(QModelIndex)),
            this,
            SLOT(setCurrent(QModelIndex)));
    m_currentElement = 0;
    m_isSetNetValue = false;
    m_model = new QStringListModel();
}

ControlForm::~ControlForm()
{
    delete ui;
}

void ControlForm::setFlowchartItems(QVector<FlowchartShape*>* flowchartItems)
{

    m_currentElement = 0;
    m_flowchartItems = flowchartItems;
    QStringList list;
    //формируем новый список
    for (int i = 0; i < m_flowchartItems->length(); ++i) {
        QString item = QString::number(i + 1)
                + " "
                + (*m_flowchartItems)[i]->typeName();
        list << item;
    }
    m_model->setStringList(list);
    ui->listView->setModel(m_model);
}

void ControlForm::setCurrent(QModelIndex index)
{
    m_isSetNetValue = true;//останавливаем события обновления
    // при получении нового элемента
    if (index.row() < m_flowchartItems->length()) {
        QString type;
        if (m_currentElement) {
            type = m_currentElement->typeName();
            if (!(type == QString("Vector") || type == QString("Line"))) {
                Block* element;
                element = static_cast<Block*>(m_currentElement);
                element->setColor(Qt::white);
            }
            else
            {
                m_currentElement->setColor(Qt::white);
            }
        }
        //извлекаем сущность
        m_currentElement = (*m_flowchartItems)[index.row()];
        type = m_currentElement->typeName();
        ui->labelType->setText(type);
        QVector2D pos;
        float x, y;
        //для каждой сущности извлекаем данные
        //и отображаем их на форме скрывая не нужное
        if (type == QString("Vector")) {
            Vector* element;
            element = static_cast<Vector*>(m_currentElement);
            ui->doubleSpinBoxAngle->setValue(element->angle());
            x = element->position().x();
            y = element->position().y();

            ui->doubleSpinBoxX->setValue(x);
            ui->doubleSpinBoxY->setValue(y);
            element->setColor(Qt::green);

            ui->plainTextEdit->setVisible(false);
            ui->groupBoxAngle->setVisible(true);
            ui->groupBoxLength->setVisible(false);

        } else if (type == QString("Line")) {
            Line* element;
            element = static_cast<Line*>(m_currentElement);

            ui->doubleSpinBoxAngle->setValue(element->angle());
            x = element->position().x();
            y = element->position().y();

            ui->doubleSpinBoxX->setValue(x);
            ui->doubleSpinBoxY->setValue(y);

            ui->spinBoxLength->setValue(element->length());
            element->setColor(Qt::green);

            ui->plainTextEdit->setVisible(false);
            ui->groupBoxAngle->setVisible(true);
            ui->groupBoxLength->setVisible(true);

        } else {
            Block* element;
            element = static_cast<Block*>(m_currentElement);
            x = element->position().x();
            y = element->position().y();

            ui->doubleSpinBoxX->setValue(x);
            ui->doubleSpinBoxY->setValue(y);
            ui->plainTextEdit->setPlainText(element->text());
            element->setColor(Qt::green);

            ui->plainTextEdit->setVisible(true);
            ui->groupBoxAngle->setVisible(false);
            ui->groupBoxLength->setVisible(false);
        }
        emit updateData();
    }
    m_isSetNetValue = false;
}

void ControlForm::updateCurrentElement()
{
    //обновляем значения текущего объекта
    if (m_currentElement) {
        QString type = m_currentElement->typeName();
        QVector2D pos;
        unsigned int length = 0;
        float angle = 0.0, x, y;
        QString text;

        x = ui->doubleSpinBoxX->value();
        y = ui->doubleSpinBoxY->value();
        angle = ui->doubleSpinBoxAngle->value();
        length = ui->spinBoxLength->value();
        text = ui->plainTextEdit->toPlainText();
        pos.setX(x);
        pos.setY(y);
        if (type == QString("Vector")) {
            Vector* element;
            element = static_cast<Vector*>(m_currentElement);
            element->setPosition(pos);
            element->setAngle(angle);
        } else if (type == QString("Line")) {
            Line* element;
            element = static_cast<Line*>(m_currentElement);
            element->setPosition(pos);
            element->setAngle(angle);
            element->setLength(length);
        } else {
            Block* element;
            element = static_cast<Block*>(m_currentElement);
            element->setPosition(pos);
            element->setText(text);
        }
        emit updateData();
    }
}
//далее обработчики изменений данных пользовательского интерфейса
void ControlForm::on_plainTextEdit_textChanged()
{
    if (!m_isSetNetValue)
        updateCurrentElement();
}

void ControlForm::on_doubleSpinBoxX_valueChanged(double arg)
{
    if (!m_isSetNetValue)
        updateCurrentElement();
}

void ControlForm::on_doubleSpinBoxY_valueChanged(double arg)
{
    if (!m_isSetNetValue)
        updateCurrentElement();
}

void ControlForm::on_doubleSpinBoxAngle_valueChanged(double arg)
{
    if (!m_isSetNetValue)
        updateCurrentElement();
}

void ControlForm::on_spinBoxLength_valueChanged(int arg)
{
    if (!m_isSetNetValue)
        updateCurrentElement();
}
