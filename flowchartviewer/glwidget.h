#ifndef GLWIDGET_H
#define GLWIDGET_H
#include <QGLWidget>
#include "flowchart.h"

class GLWidget : public QGLWidget {
    Q_OBJECT

public:
    explicit GLWidget(QWidget* parent = 0, QGLWidget* shareWidget = 0);
    ~GLWidget();
    void loadFileAlgoritm(const QString& src);
    void saveFileAlgoritm(const QString& src);
    QVector<FlowchartShape*>* flowchartItems();
    void setFlowchartItems(const QVector<FlowchartShape*>& flowchartItems);
    void clearFlowchartItems();

    void keyPressEvent(QKeyEvent* event);
signals:
    void flowchartUpdate();
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
private:
    QMatrix4x4 m_projection;
    QMatrix4x4 m_view;
    QMatrix4x4 m_viewTransform;
    QVector<FlowchartShape*> m_flowchartItems;
    float aspectRatio;
    QVector3D pos;
    QVector3D center;
};

#endif
