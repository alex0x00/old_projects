#include <QtWidgets>
#include <QtOpenGL>
#include <QTextStream>
#include "glwidget.h"

GLWidget::GLWidget(QWidget* parent, QGLWidget* shareWidget)
    : QGLWidget(parent, shareWidget)
{
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(16);
    setFormat(fmt);
    pos = QVector3D(0, 0, 15);
    center = QVector3D(0, 0, 0);
}

GLWidget::~GLWidget()
{
    clearFlowchartItems();
}

void GLWidget::initializeGL()
{
    //настройка opengl и камеры
    m_viewTransform.setToIdentity();
    aspectRatio = width() / height();
    m_projection.perspective(60, aspectRatio, 1, 1000);

    m_view.lookAt(pos,
                  center,
                  QVector3D(0, 1, 0));

    glEnable(GL_DEPTH_TEST);
}

void GLWidget::paintGL()
{
    glClearColor(0.3, 0.3, 0.3, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);

    QMatrix4x4 vp, block;

    vp = m_projection * m_view * m_viewTransform;

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    for (int i = 0; i < m_flowchartItems.length(); ++i) {
        m_flowchartItems[i]->draw();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    //изменяем настройки порта вывода графики
    glViewport(0, 0, width, height);
    aspectRatio = width / height;
    m_projection.setToIdentity();
    m_projection.perspective(60, aspectRatio, 1, 1000);
    updateGL();//обновляем
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    //обрабатываем события нажатия клавиш
    float step = 0.5f;
    float angle = 5.0f;
    //выходим по esc
    if (event->key() == Qt::Key_8) {
        pos += QVector3D(0.0, step, 0.0);
        center += QVector3D(0.0, step, 0.0);
    } else if (event->key() == Qt::Key_2) {
        pos += QVector3D(0.0, -step, 0.0);
        center += QVector3D(0.0, -step, 0.0);
    } else if (event->key() == Qt::Key_4) {
        pos += QVector3D(step, 0.0, 0.0);
        center += QVector3D(step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_6) {
        pos += QVector3D(-step, 0.0, 0.0);
        center += QVector3D(-step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_Plus) {
        pos += QVector3D(0.0, 0.0, -step);
    } else if (event->key() == Qt::Key_Minus) {
        pos += QVector3D(0.0, 0.0, step);
    }
    m_view.setToIdentity();
    m_view.lookAt(pos,
                  center,
                  QVector3D(0, 1, 0));
    updateGL();
}

QVector<FlowchartShape*>* GLWidget::flowchartItems()
{
    return &m_flowchartItems;
}
void GLWidget::setFlowchartItems(const QVector<FlowchartShape*>& flowchartItems)
{
    m_flowchartItems = flowchartItems;
    emit flowchartUpdate();
}

void GLWidget::clearFlowchartItems()
{
    foreach(FlowchartShape * item, m_flowchartItems)
    {
        delete item;
    }
    m_flowchartItems.clear();
    emit flowchartUpdate();
    updateGL();
}

void GLWidget::loadFileAlgoritm(const QString& src)
{
    //загрузка файла с алгоритмом
    QString path_src = src;

    if (!QFile::exists(path_src)) {
        qWarning() << "Algoritm file not found:" << path_src;
        return;
    }
    enum AlgoritmBloksFileFormat {
        NONE = 0,
        BLOCK_BEGIN_END,
        BLOCK_ACTION,
        BLOCK_CONDITIONAL,
        BLOCK_INPUT_OUTPUT,
        VECTOR,
        LINE
    } state = NONE;
    QFile file(path_src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Algoritm file not opened:";
        return;
    }
    QString line;
    QVector<QString> tokens;
    FlowchartShape* element = 0;
    QString text;
    QVector2D pos;
    unsigned int length;
    float angle;

    while (!file.atEnd()) {
        line = file.readLine();
        tokens = parse_tokens(line);
        if (tokens.length() == 0) {//если пустая строка
            continue;
        }
        if (tokens[0] == "begin") {

            if (tokens[1] == "BeginEnd") {
                state = BLOCK_BEGIN_END;
            } else if (tokens[1] == "Conditional") {
                state = BLOCK_CONDITIONAL;
            } else if (tokens[1] == "Action") {
                state = BLOCK_ACTION;
            } else if (tokens[1] == "InputOutput") {
                state = BLOCK_INPUT_OUTPUT;
            } else if (tokens[1] == "Vector") {
                state = VECTOR;
            } else if (tokens[1] == "Line") {
                state = LINE;
            }
        } else if (tokens[0] == "pos") {
            float x, y;
            x = tokens[1].toFloat();
            y = tokens[2].toFloat();
            pos.setX(x);
            pos.setY(y);
        } else if (tokens[0] == "text") {
            text = line.remove(0, 5);// удаляем text
            //text.replace(QString("\\n"), QString("\n"));
        } else if (tokens[0] == "angle") {
            angle = tokens[1].toFloat();
        } else if (tokens[0] == "length") {
            length = tokens[1].toUInt();
        } else if (tokens[0] == "end") {
            //описаение объекта завершено, сохраняем
            element = 0;
            switch (state) {
            case NONE:
                break;
            case BLOCK_BEGIN_END:
                element = new BlockBeginEnd(pos, text);
                break;
            case BLOCK_CONDITIONAL:
                element = new BlockConditional(pos, text);
                break;
            case BLOCK_ACTION:
                element = new BlockAction(pos, text);
                break;
            case BLOCK_INPUT_OUTPUT:
                element = new BlockInputOutput(pos, text);
                break;
            case VECTOR:
                element = new Vector(pos, angle);
                break;
            case LINE:
                element = new Line(pos, angle, length);
                break;
            }
            if (element != 0) {
                m_flowchartItems.push_back(element);
            }
        }
    }
    emit flowchartUpdate();
    qDebug() << "Load file algoritm";
}

void GLWidget::saveFileAlgoritm(const QString &src)
{
    QString path_src = src;

    QFile file(path_src);
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning() << "Algoritm file not opened:";
        return;
    }

    foreach (FlowchartShape *item, m_flowchartItems) {
        file.write(item->toString().toUtf8());
        //out << item->toString();
    }
    qDebug() << "Save file algoritm";
}
