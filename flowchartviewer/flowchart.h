#ifndef FLOWCHART_H
#define FLOWCHART_H

#include <QCoreApplication>
#include <QOpenGL.h>
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector2D>
#include <QVector>
#include <QImage>
#include <QColor>
#include <QString>
#include <QMap>
#include <QMapIterator>

void checkError(int line = 0);

//разбивает по пробельным символам строку в массив строк
QVector<QString>& parse_tokens(const QString& str);

//хранилище (вершин) геометрии
class Item {
public:
    Item()
    {
    }
    ~Item()
    {
        pos.clear();
        norm.clear();
    }
    QVector<QVector3D> pos; //вершины
    QVector<QVector3D> norm; //нормали
};
//базовай класс элементнов фигур
class FlowchartShape {
public:
    FlowchartShape(const QString& src);
    FlowchartShape();
    virtual ~FlowchartShape();
    virtual void draw()
    {
        qDebug() << "AlgoritmFlowchart.draw()";
    }
    virtual QString toString() = 0;
    QString path() const;
    void setPath(const QString& path);
    Item* item(const QString& name);
    QMatrix4x4 transform() const;
    void setTransform(const QMatrix4x4& transform);
    QVector2D position() const;
    void setPosition(const QVector2D& position);
    QColor color() const;
    void useColor();
    void setColor(QColor arg);
    QString typeName() const;
    void setTypeName(const QString &typeName);

protected:
    //кеш загруженной графики
    static QMap<QString, Item*>* m_storeItems; // = 0;
    static unsigned int m_counterItems; //счетчик объектов для очистки хранилища
private:
    Item* loadObject3D(QString src);
    QString m_path;
    QMatrix4x4 m_transform;
    QVector2D m_position;
    QColor m_color;
    QString m_typeName;
};
//класс стрелки
class Vector : public FlowchartShape {
public:
    Vector(const QVector2D& pos, float angle);
    void draw();
    QString toString();

    float angle() const;
    void setAngle(float angle);
    void setPosition(const QVector2D& pos);
private:
    float m_angle;
};
//класс линии
class Line : public FlowchartShape {
public:
    Line(const QVector2D& pos, float angle, unsigned int length);
    void draw();
    QString toString();

    unsigned int length() const;
    void setLength(unsigned int length);

    float angle() const;
    void setAngle(float angle);
    void setPosition(const QVector2D& pos);

private:
    unsigned int m_length;
    float m_angle;
};
//базовый класс блочных фигур
class Block : public FlowchartShape {
public:
    Block(QString src, QString text);
    ~Block();
    QMatrix4x4 textureTranslate() const;
    void setTextureTranslate(const QMatrix4x4& textureTranslate);
    QString text() const;
    void setText(const QString& text);
    QString toString(const QString& typeBlock);
    void setColor(QColor arg);
    QColor colorText() const;
    void setColorText(const QColor& colorText);
    void setPosition(const QVector2D& pos);

protected:
    void drawBlockText();
    void drawBlock();
private:
    QImage createSurfaceText(const QString& text,
                             QColor background,
                             QColor colorText);
    void prepareTexture(const QImage& prepare,
                        GLuint& texture,
                        GLenum target = GL_TEXTURE_2D,
                        GLint format = GL_RGBA);
    void swapTexture();
    void createTexture();
    GLuint m_textureId;
    QMatrix4x4 m_textureTranslate;
    QString m_text;
    QColor m_colorText;
};
//класс блока Начала или Конца
class BlockBeginEnd : public Block {
public:
    BlockBeginEnd(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QString toString()
    {
        return Block::toString(QString("BeginEnd"));
    }
};
//класс блока Условия
class BlockConditional : public Block {
public:
    BlockConditional(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QString toString()
    {
        return Block::toString(QString("Conditional"));
    }
};
//класс блока Действия
class BlockAction : public Block {
public:
    BlockAction(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QString toString()
    {
        return Block::toString(QString("Action"));
    }
};
//класс блока Ввод или Вывод
class BlockInputOutput : public Block {
public:
    BlockInputOutput(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QString toString()
    {
        return Block::toString(QString("InputOutput"));
    }
};
#endif // FLOWCHART_H
