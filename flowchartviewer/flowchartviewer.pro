HEADERS = glwidget.h \
        flowchart.h \
    controlform.h \
    mainwindow.h
SOURCES = glwidget.cpp \
        main.cpp \
        flowchart.cpp \
    controlform.cpp \
    mainwindow.cpp
QT += opengl widgets
LIBS += -lopengl32
FORMS += \
    controlform.ui \
    mainwindow.ui

RESOURCES += \
    res.qrc
