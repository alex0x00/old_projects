#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //соединяем генераторы событий с их обработчиками
    connect(ui->actionLoad, SIGNAL(triggered()),
            this, SLOT(openDialogFileLoad()));

    connect(ui->actionSave, SIGNAL(triggered()),
            this, SLOT(openDialogFileSave()));

    connect(ui->actionExit, SIGNAL(triggered()),
            this, SLOT(exit()));

    connect(ui->actionClear, SIGNAL(triggered()),
            this, SLOT(clearWorkSpace()));

    connect(ui->actionEdit, SIGNAL(triggered()),
            this, SLOT(showControl()));

    m_controlForm = new ControlForm();

    m_controlForm->setFlowchartItems(flowchartItems());
    connect(ui->glwidget,
            SIGNAL(flowchartUpdate()),
            this,
            SLOT(updateControlInformation()));

    connect(m_controlForm,
            SIGNAL(updateData()),
            ui->glwidget,
            SLOT(updateGL()));
}

void MainWindow::loadFileAlgoritm(const QString &src)
{
    ui->glwidget->loadFileAlgoritm(src);
}

void MainWindow::saveFileAlgoritm(const QString &src)
{
    ui->glwidget->saveFileAlgoritm(src);
}

QVector<FlowchartShape *>* MainWindow::flowchartItems()
{
    return ui->glwidget->flowchartItems();
}

void MainWindow::clearFlowchartItems()
{
    ui->glwidget->clearFlowchartItems();
}
//передаём все события нажатий клавиш другому классу
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    ui->glwidget->keyPressEvent(event);
}

MainWindow::~MainWindow()
{
    delete ui;
}
//открывают диалоги
void MainWindow::openDialogFileLoad()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Файл алгоритма"),
                                                    "./",
                                                    tr("Text (*.txt)"));
    loadFileAlgoritm(fileName);
}

void MainWindow::openDialogFileSave()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Файл алгоритма"),
                                                    "./",
                                                    tr("Text (*.txt)"));
    saveFileAlgoritm(fileName);
}

void MainWindow::exit()
{
    QApplication::exit(0);
}

void MainWindow::clearWorkSpace()
{
    clearFlowchartItems();
}

void MainWindow::showControl()
{
    m_controlForm->show();
}

void MainWindow::updateGLWidget()
{
    ui->glwidget->updateGL();
}

void MainWindow::updateControlInformation()
{
    m_controlForm->setFlowchartItems(flowchartItems());
}
