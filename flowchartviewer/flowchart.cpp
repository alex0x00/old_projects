#include "flowchart.h"
#include <QPainter>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QGLWidget>
//устанавливаем стандартные значения
QMap<QString, Item*>* FlowchartShape::m_storeItems = 0;
unsigned int FlowchartShape::m_counterItems = 0;

Block::Block(QString src, QString text)
    : FlowchartShape(src)
{
    Item* currentItem = item(path());
    m_textureId = 0;
    float eps = 1.0E-3;
    //подгоняем объекты, чтобы глубина была одинаковой
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        if (currentItem->pos[i].z() > 0)
            currentItem->pos[i].setZ(0.5 - eps);
        else
            currentItem->pos[i].setZ(-0.5 + eps);
    }
    m_text = text;
    createTexture();
}

Block::~Block()
{
    glDeleteTextures(1, &m_textureId); // удаляем текстурные объекты
}

void Block::drawBlockText()
{
    //отрисовывает квадрат с текстурой
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_textureId);

    glPushMatrix();
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glRotatef(-90, 0.0, 0.0, 1.0);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());

    glBegin(GL_QUADS);
    glVertex3f(-1.0, -1.0, 0.5);
    glTexCoord2f(0.01, 0.01);

    glVertex3f(-1.0, 1.0, 0.5);
    glTexCoord2f(0.01, 0.99);

    glVertex3f(1.0, 1.0, 0.5);
    glTexCoord2f(0.99, 0.99);

    glVertex3f(1.0, -1.0, 0.5);
    glTexCoord2f(0.99, 0.01);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

void Block::drawBlock()
{
    //получаем графику из хранилища
    Item* currentItem = item(path());
    useColor();
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    //трасформация объекта согласно его свойству pos
    glLoadMatrixf(transform().data());
    //отрисовываем
    glBegin(GL_TRIANGLES);
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        //glColor3f(1.0, 1.0, 1.0);
        glVertex3f(
                    currentItem->pos[i].x(),
                    currentItem->pos[i].y(),
                    currentItem->pos[i].z());
#if defined(USE_NORMALS)
        glNormal3f(
                    currentItem->norm[i].x(),
                    currentItem->norm[i].y(),
                    currentItem->norm[i].z());
#endif
    }
    glEnd();
}

QImage Block::createSurfaceText(const QString &text, QColor background, QColor colorText)
{
    //создаем текстуру с нашим изображением
    //ресуем на изображении
    unsigned int size = 256; //размер квадратной текстуры
    QImage* image = new QImage(size, size, QImage::Format_ARGB32);
    QPainter painter;
    QFont font;
    font.setPixelSize(34);
    painter.begin(image);
    painter.setBrush(QBrush(background));
    painter.drawRect(QRect(0, 0, size, size));
    painter.setFont(font);
    painter.setPen(QPen(colorText));
    painter.drawText(QRect(0, 0, size, size), Qt::AlignCenter, text);
    painter.end();
    QImage prepare;
    //преобразуем изображение в формат для opengl
    prepare = QGLWidget::convertToGLFormat(*image);
    delete image;
    return prepare;
}

void Block::prepareTexture(const QImage &prepare, GLuint &texture, GLenum target, GLint format)
{
    glBindTexture(target, texture); //используем этот текстурный
    //индекс  как текущий
    //передаем данные
    glTexImage2D(target,
                 0,
                 format,
                 prepare.width(),
                 prepare.height(),
                 0,
                 format,
                 GL_UNSIGNED_BYTE,
                 prepare.bits());
    //checkError(__LINE__);
    //параметры фильтрации
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Block::swapTexture()
{
    QImage image = createSurfaceText(m_text,
                                     color(),
                                     m_colorText);
    prepareTexture(image, m_textureId);
}

void Block::createTexture()
{
    QImage image = createSurfaceText(m_text,
                                     color(),
                                     m_colorText);
    glGenTextures(1, &m_textureId); //запрашиваем индекс текстуры
    prepareTexture(image, m_textureId);
}
QColor Block::colorText() const
{
    return m_colorText;
}

void Block::setColorText(const QColor& colorText)
{
    m_colorText = colorText;
    swapTexture();
}

void Block::setPosition(const QVector2D &pos)
{
    FlowchartShape::setPosition(pos);
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(pos.x(),
                     pos.y());
    setTransform(matrix);
}

FlowchartShape::FlowchartShape(const QString& src)
{
    m_path = src;
    m_color = Qt::white;
    if (m_storeItems == 0)
        m_storeItems = new QMap<QString, Item*>;
    //если не была загружена ранее
    if (!m_storeItems->contains(m_path)) {
        //загружаем и добавляем новую графику
        Item* item = loadObject3D(m_path);
        m_storeItems->insert(m_path, item);
    }
    m_counterItems++;
}

FlowchartShape::FlowchartShape()
{
    m_path = QString("<none>");
    m_color = Qt::white;
    if (m_storeItems == 0)
        m_storeItems = new QMap<QString, Item*>;
    m_counterItems++;
}

FlowchartShape::~FlowchartShape()
{
    m_counterItems--;
    if (m_counterItems == 0) {
        //елсли последний объект удаляем хранилеще
        QMapIterator<QString, Item*> i(*m_storeItems);
        Item* item;
        while (i.hasNext()) {
            i.next();
            item = i.value();
            delete item;
        }
        m_storeItems->clear();
        qDebug() << "delete all items in store objects";
    }
}

QString FlowchartShape::path() const
{
    return m_path;
}

void FlowchartShape::setPath(const QString& path)
{
    m_path = path;
}

Item* FlowchartShape::item(const QString& name)
{
    if (m_storeItems->contains(m_path)) {
        return m_storeItems->value(name);
    } else {
        return 0;
    }
}

QMatrix4x4 FlowchartShape::transform() const
{
    return m_transform;
}

void FlowchartShape::setTransform(const QMatrix4x4& transform)
{
    m_transform = transform;
}

Item* FlowchartShape::loadObject3D(QString src)
{
    //загружает графику из файла
    enum ReadState {
        NONE = 0,
        POSITION,
        NORMAL
    } state;

    if (!QFile::exists(src)) {
        QString path_src = QCoreApplication::applicationDirPath() + "/" + src;
        if (!QFile::exists(path_src)) {
            qWarning() << "File not found:" << path_src;
            return 0;
        } else {
            src = path_src;
        }
    }
    QFile file(src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "File not opened:";
    }
    QString line;
    QVector<QString> tokens;
    Item* item = new Item;

    state = NONE;
    while (!file.atEnd()) {
        line = file.readLine();
        tokens = parse_tokens(line);
        if (tokens[0] == "position") {
            state = POSITION;
        } else if (tokens[0] == "normal") {
            state = NORMAL;
        } else if (state != NONE) {
            QTextStream in(line.toUtf8());
            float x, y, z;
            in >> x >> y >> z;
            if (state == POSITION) {
                item->pos.push_back(QVector3D(x, y, z));
            } else if (state == NORMAL) {
                item->norm.push_back(QVector3D(x, y, z));
            }
        }
    }
    qDebug() << "Load model";
    return item;
}
QString FlowchartShape::typeName() const
{
    return m_typeName;
}

void FlowchartShape::setTypeName(const QString &typeName)
{
    m_typeName = typeName;
}

QVector2D FlowchartShape::position() const
{
    return m_position;
}

void FlowchartShape::setPosition(const QVector2D& position)
{
    m_position = position;
}

QColor FlowchartShape::color() const
{
    return m_color;
}

void FlowchartShape::useColor()
{
    glColor3f(m_color.redF(),
              m_color.greenF(),
              m_color.blueF());
}

void FlowchartShape::setColor(QColor arg)
{
    if (m_color != arg) {
        m_color = arg;
    }
}

QString Block::text() const
{
    return m_text;
}

void Block::setText(const QString& text)
{
    m_text = text;
    swapTexture();
}

QString Block::toString(const QString& typeBlock)
{
    QString ret;
    ret += QString("\r\nbegin ")
            + typeBlock
            + QString("\r\npos ")
            + QString::number(position().x())
            + QString(" ")
            + QString::number(position().y())
            + QString("\r\ntext ")
            //+QString(m_text).replace(QString("\n"), QString("\\n"))
            + m_text
            + QString("\r\nend\r\n");
    return ret;
}

void Block::setColor(QColor arg)
{
    if (color() != arg) {
        FlowchartShape::setColor(arg);
        swapTexture();
    }
}

QVector<QString>& parse_tokens(const QString& str)
{
    QVector<QString>* tokens = new QVector<QString>;

    int begin = 0, end = 0, length = str.length();
    bool is_word = false;

    for (int i = 0; i < length; ++i) {
        if (str[i].isSpace()) //конец токена
        {
            if (is_word) {
                end = i;
                is_word = false;
                tokens->push_back(str.mid(begin, end - begin));
            }
        } else {
            if (!is_word) {
                begin = i;
                is_word = true; //начало токена
            }
        }
    }

    if (is_word) {
        end = length;
        tokens->push_back(str.mid(begin, end - begin));
    }
    return *tokens;
}
QMatrix4x4 Block::textureTranslate() const
{
    return m_textureTranslate;
}

void Block::setTextureTranslate(const QMatrix4x4& textureTranslate)
{
    m_textureTranslate = textureTranslate;
}

void checkError(int line)
{
    GLuint isError = glGetError();
    if (isError != GL_NO_ERROR) {
        qDebug() << "Error OpenGL code:"
                 << QString::number(isError, 16)
                 << endl
                 << "Line:"
                 << line;
    }
}

BlockBeginEnd::BlockBeginEnd(const QVector2D& pos, const QString& text)
    : Block(":/boxs/begin_end._obj", text)
{
    setTypeName(QString("BeginEnd"));
    setPosition(pos);
}

BlockConditional::BlockConditional(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/condition._obj"), text)
{
    setTypeName(QString("Conditional"));
    setPosition(pos);
}

BlockAction::BlockAction(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/action._obj"), text)
{
    setTypeName(QString("Action"));
    setPosition(pos);
}

BlockInputOutput::BlockInputOutput(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/input_output._obj"), text)
{
    setTypeName(QString("InputOutput"));
    setPosition(pos);
}

Vector::Vector(const QVector2D& pos, float angle)
    : FlowchartShape(QString(":/boxs/vector._obj"))
{
    setTypeName(QString("Vector"));
    setPosition(pos);
    m_angle = angle;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(pos.x(),
                     pos.y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Vector::draw()
{
    Item* currentItem = item(path());
    useColor();
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());
    glBegin(GL_TRIANGLES);
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        //glColor3f(1.0, 1.0, 1.0);
        glVertex3f(
                    currentItem->pos[i].x(),
                    currentItem->pos[i].y(),
                    currentItem->pos[i].z());

#if defined(USE_NORMALS)
        glNormal3f(
                    currentItem->norm[i].x(),
                    currentItem->norm[i].y(),
                    currentItem->norm[i].z());
#endif
    }
    glEnd();
}

QString Vector::toString()
{
    QString ret;
    ret += QString("\r\nbegin Vector")
            + QString("\r\npos ")
            + QString::number(position().x())
            + QString(" ")
            + QString::number(position().y())
            + QString("\r\nangle ")
            + QString::number(m_angle)
            + QString("\r\nend\r\n");
    return ret;
}
float Vector::angle() const
{
    return m_angle;
}

void Vector::setAngle(float angle)
{
    m_angle = angle;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(position().x(),
                     position().y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Vector::setPosition(const QVector2D &pos)
{
    FlowchartShape::setPosition(pos);
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(pos.x(),
                     pos.y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}


Line::Line(const QVector2D& pos, float angle, unsigned int length)
{
    setTypeName(QString("Line"));
    setPosition(pos);
    m_angle = angle;
    m_length = length;

    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(pos.x(),
                     pos.y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Line::draw()
{
    float size = 0.3;
    useColor();
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(0.0, size, size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    //левая грань
    glVertex3f(0.0, size, -size);
    glVertex3f(0.0, size, size);
    glVertex3f(0.0, -size, size);
    glVertex3f(0.0, -size, -size);
    // задняя грань
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(0.0, -size, -size);
    //правая грань
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(1.0 * m_length, -size, -size);

    //верх
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(0.0, size, size);
    //низ
    glVertex3f(0.0, -size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    glEnd();
}

QString Line::toString()
{
    QString ret;
    ret += QString("\r\nbegin Line")
            + QString("\r\npos ")
            + QString::number(position().x())
            + QString(" ")
            + QString::number(position().y())
            + QString("\r\nangle ")
            + QString::number(m_angle)
            + QString("\r\nlength ")
            + QString::number(m_length)
            + QString("\r\nend\r\n");
    return ret;
}
unsigned int Line::length() const
{
    return m_length;
}

void Line::setLength(unsigned int length)
{
    m_length = length;
}
float Line::angle() const
{
    return m_angle;
}

void Line::setAngle(float angle)
{
    m_angle = angle;
    m_angle = angle;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(position().x(),
                     position().y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Line::setPosition(const QVector2D &pos)
{
    FlowchartShape::setPosition(pos);
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(pos.x(),
                     pos.y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}


