#ifndef CONTROLFORM_H
#define CONTROLFORM_H

#include <QWidget>
#include "flowchart.h"
#include <QModelIndex>
#include <QStringListModel>

namespace Ui {
class ControlForm;
}

class ControlForm : public QWidget {
    Q_OBJECT

public:
    explicit ControlForm(QWidget* parent = 0);
    ~ControlForm();
    void updateCurrentElement();
signals:
    void updateData();

public slots:
    void setFlowchartItems(QVector<FlowchartShape*>* flowchartItems);
    void setCurrent(QModelIndex index);

private slots:
    void on_plainTextEdit_textChanged();

    void on_doubleSpinBoxX_valueChanged(double);

    void on_doubleSpinBoxY_valueChanged(double);

    void on_doubleSpinBoxAngle_valueChanged(double);

    void on_spinBoxLength_valueChanged(int);

private:
    Ui::ControlForm* ui;
    QVector<FlowchartShape*>* m_flowchartItems;
    FlowchartShape* m_currentElement;
    bool m_isSetNetValue;
    QStringListModel* m_model;
};

#endif // CONTROLFORM_H
