#ifndef FLOWCHART_H
#define FLOWCHART_H

#include <QOpenGL.h>
#include <qopengl.h>
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector2D>
#include <QVector>
#include <QImage>
#include <QString>


//базовый класс элементов фигур
class AlgoritmBlockScheme {
public:
    enum AlgoritmBlockSchemeType {
        Undefined = -1,
        Arrow = 0,
        Line = 1,
        Begin,
        End,
        Function,
        Action,
        Conditional,
        InputOutput,
        Loop
    };
    AlgoritmBlockScheme(AlgoritmBlockSchemeType type, const QVector2D& pos, const QString& text);

    AlgoritmBlockScheme(AlgoritmBlockSchemeType type, const QVector2D& pos, float angle, float length);
    QMatrix4x4 transform() const;

    void setTransform(const QMatrix4x4& transform);

    QVector2D position() const;

    void setPosition(const QVector2D& position);

    void draw();

private:
    void updateTransform();

    void createTextureText(const QString& text);
    void drawLine();
    void drawArrow();
    void drawBlockText();
    void drawBeginEnd();
    void drawInputOutput();
    void drawConditional();
    void drawAction();
    void drawLoop();

    QString m_typeName;
    AlgoritmBlockSchemeType m_type;
    QMatrix4x4 m_transform;
    QVector2D m_position;
    float m_angle;
    float m_length;
    GLuint m_texture;
};

#endif // FLOWCHART_H
