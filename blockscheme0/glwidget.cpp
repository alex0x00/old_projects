
#include <QtWidgets>
#include <QtOpenGL>

#include "glwidget.h"

GLWidget::GLWidget(QWidget* parent, QGLWidget* shareWidget)
    : QGLWidget(parent, shareWidget)
{
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4); //количество пикселей на сглаживание одного
    setFormat(fmt);
    m_pos = QVector3D(0, 0, 20);
    m_center = QVector3D(0, 0, 0);
}

GLWidget::~GLWidget()
{
}

void GLWidget::initializeGL()
{
    //установка значениями
    m_viewTransform.setToIdentity();
    m_aspectRatio = width() / height();
    m_projection.perspective(60, m_aspectRatio, 1, 100);

    m_view.lookAt(m_pos,
                  m_center,
                  QVector3D(0, 1, 0));

    glEnable(GL_DEPTH_TEST);
}
//метод отрисовки
void GLWidget::paintGL()
{
    glClearColor(0.3, 0.3, 0.3, 1.0); //серый фон
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    QMatrix4x4 vp;
    //устанавливаем результирующую матрицу проекции
    vp = m_projection * m_view * m_viewTransform;

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data()); //передам матрицу opengl
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //отрисовка объектов
    for (int i = 0; i < m_schemes.length(); ++i) {
        m_schemes[i]->draw();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    //изменяем настройки порта вывода графики
    glViewport(0, 0, width, height);
    m_aspectRatio = width / height;

    m_projection.setToIdentity();
    m_projection.perspective(60, m_aspectRatio, 1, 100);
    updateGL(); //обновляем
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    float step = 1.0f;
    //выходи по escape
    //управление камерой перемещение вращение
    if (event->key() == Qt::Key_O) {
        //открывам диалог на выбор файла
        QString fileName = QFileDialog::getOpenFileName(this, tr("Открыть файл описания алгоритма"),
                                                        "./",
                                                        tr("Text (*.txt)"));
        algoritmLoad(fileName);
    } else if (event->key() == Qt::Key_C) {
        m_schemes.clear();
    } else if (event->key() == Qt::Key_Escape) {
        exit(0);
    } else if (event->key() == Qt::Key_8) {
        m_pos += QVector3D(0.0, step, 0.0);
        m_center += QVector3D(0.0, step, 0.0);
    } else if (event->key() == Qt::Key_2) {
        m_pos += QVector3D(0.0, -step, 0.0);
        m_center += QVector3D(0.0, -step, 0.0);
    } else if (event->key() == Qt::Key_4) {
        m_pos += QVector3D(step, 0.0, 0.0);
        m_center += QVector3D(step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_6) {
        m_pos += QVector3D(-step, 0.0, 0.0);
        m_center += QVector3D(-step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_Plus) {
        m_pos += QVector3D(0.0, 0.0, -step);
    } else if (event->key() == Qt::Key_Minus) {
        m_pos += QVector3D(0.0, 0.0, step);
    } else if (event->key() == Qt::Key_Up) {
        m_viewTransform.rotate(-step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Down) {
        m_viewTransform.rotate(step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Left) {
        m_viewTransform.rotate(-step, QVector3D(0, 1, 0));
    } else if (event->key() == Qt::Key_Right) {
        m_viewTransform.rotate(step, QVector3D(0, 1, 0));
    }

    m_view.setToIdentity();
    //формируем навую матрицу точки обзора
    m_view.lookAt(m_pos,
                  m_center,
                  QVector3D(0, 1, 0));
    updateGL(); //обновляем
}

void GLWidget::addObject(const AlgoritmBlockScheme::AlgoritmBlockSchemeType state, const QVector2D &pos, const QString &text, float angle, float length)
{
    //описаение объекта завершено, сохраняем
    AlgoritmBlockScheme* element = 0;
    switch (state) {
    case AlgoritmBlockScheme::Begin:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Begin, pos, text);
        break;
    case AlgoritmBlockScheme::End:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::End, pos, text);
        break;
    case AlgoritmBlockScheme::Function:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Function, pos, text);
        break;
    case AlgoritmBlockScheme::Conditional:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Conditional, pos, text);
        break;
    case AlgoritmBlockScheme::Action:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Action, pos, text);
        break;
    case AlgoritmBlockScheme::Loop:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Loop, pos, text);
        break;
    case AlgoritmBlockScheme::InputOutput:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::InputOutput, pos, text);
        break;
    case AlgoritmBlockScheme::Arrow:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Arrow, pos, angle, length);
        break;
    case AlgoritmBlockScheme::Line:
        element = new AlgoritmBlockScheme(AlgoritmBlockScheme::Line, pos, angle, length);
        break;
    }
    if (element != 0) {
        m_schemes.push_back(element); //добавляем новый элемент
    }
}



void GLWidget::algoritmLoad(const QString& src)
{
    //загрузка файла с алгоритмом
    QString path_src = src;
    if (!QFile::exists(path_src)) {
        qWarning() << "Error find file:" << path_src;
        return;
    }
    AlgoritmBlockScheme::AlgoritmBlockSchemeType state = AlgoritmBlockScheme::Undefined;

    QFile file(path_src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Error opened file:";
    }
    QString line;
    QStringList words;
    QString text;
    QVector2D pos;
    float length;
    float angle;

    while (!file.atEnd()) {
        line = file.readLine();
        words = line.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        if (words.length() == 0) {
            continue; //если пустая строка
        }
        if (words[0] == "Начало") {
            text = "";
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Begin;
        } else if (words[0] == "Конец") {
            text = "";
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::End;
        } else if (words[0] == "Фунция" || words[0] == "Процедура") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Function;
            text = "";
        } else if (words[0] == "Условие") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Conditional;
            text = "";
        } else if (words[0] == "Действие") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Action;
            text = "";
        } else if (words[0] == "Цикл") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Loop;
            text = "";
        } else if (words[0] == "Ввод") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::InputOutput;
            text = "";
        } else if (words[0] == "Вывод") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::InputOutput;
            text = "";
        } else if (words[0] == "Стрелка") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Arrow;
            angle = length = 0.0;
        } else if (words[0] == "Линия") {
            addObject(state, pos, text, angle, length);
            state = AlgoritmBlockScheme::Line;
            angle = length = 0.0;
        } else if (words[0] == "позиция") {
            float x, y;
            x = words[1].toFloat();
            y = words[2].toFloat();
            pos.setX(x);
            pos.setY(y);
        } else if (words[0] == "описание" && state > 1) {
            text = line.remove(0, QString("описание ").length()); // удаляем описание
        } else if (words[0] == "угол" && state <= 1) {
            angle = words[1].toFloat();
        } else if (words[0] == "длина" && state <= 1) {
            length = words[1].toFloat();
        }
    }
    addObject(state, pos, text, angle, length);
    qDebug() << "Load file algoritm \n Loading objects:" << m_schemes.length();
}
