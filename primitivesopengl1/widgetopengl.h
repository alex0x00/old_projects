#ifndef WIDGETOPENGL_H
#define WIDGETOPENGL_H
#include <QGLWidget>
#include <QVector3D>
#include <QVector2D>
#include <QMatrix4x4>

class WidgetOpenGL : public QGLWidget {
public:
    explicit WidgetOpenGL(QWidget* parent);
    ~WidgetOpenGL();
    void move(const QVector3D& traslate);

    double radius() const;
    void setRadius(double radius);

    GLenum primitivesMode() const;
    void setPrimitivesMode(const QString& mode);

    float sizePointsAndLines() const;
    void setSizePointsAndLines(float sizePointsAndLines);

    unsigned int pointsCount() const;
    void setPointsCount(unsigned int pointsCount);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    QVector2D m_horizontal, m_vertical;
    QMatrix4x4 m_projection;
    unsigned int m_pointsCount;
    double m_radius;
    GLenum m_primitivesMode;
    float m_sizePointsAndLines;
};

#endif // WIDGETOPENGL_H
