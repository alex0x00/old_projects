#include "widgetopengl.h"
#include <math.h>
#include <qopengl.h>
#include <QtOpenGL>
#include <QOpenGL.h>
#include <qopengl.h>
#include <QGLWidget>

WidgetOpenGL::WidgetOpenGL(QWidget* parent)
    : QGLWidget(parent)
{
    float size = 20;

    m_horizontal.setX(-size*4.0/3.0);
    m_horizontal.setY(size*4.0/3.0);
    m_vertical.setX(-size*3.0/4.0);
    m_vertical.setY(size*3.0/4.0);

    m_projection.setToIdentity();
    m_projection.ortho(m_horizontal.x(), m_horizontal.y(),
                       m_vertical.x(), m_vertical.y(),
                       -10.0, 10.0);

    m_pointsCount = 12;
    m_radius = 1.0;
    m_sizePointsAndLines = 1.0;
    m_primitivesMode = GL_POINTS;
}

WidgetOpenGL::~WidgetOpenGL()
{
}

void WidgetOpenGL::move(const QVector3D& traslate)
{
    float size = 20;

    m_horizontal.setX(-size*4.0/3.0);
    m_horizontal.setY(size*4.0/3.0);
    m_vertical.setX(-size*3.0/4.0);
    m_vertical.setY(size*3.0/4.0);


    m_horizontal += QVector2D(traslate.x(), traslate.x());
    m_vertical += QVector2D(traslate.y(), traslate.y());
    m_projection.setToIdentity();
    m_projection.ortho(m_horizontal.x(), m_horizontal.y(),
                       m_vertical.x(), m_vertical.y(),
                       -10.0, 10.0);
    updateGL();
}

void WidgetOpenGL::initializeGL()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);

    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
}

void WidgetOpenGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPointSize(m_sizePointsAndLines);
    glLineWidth(m_sizePointsAndLines);
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(m_projection.data()); //передам матрицу opengl
    glMatrixMode(GL_MODELVIEW);
    glColor3f(1.0, 1.0, 1.0);
    float angle = 360.0, tmp;
    float step = angle / (float)m_pointsCount;

    glBegin(m_primitivesMode);
    for (; angle > 0.0; angle -= step) {
        tmp = M_PI * angle / 180.0;
        glVertex3f(m_radius * cos(tmp), m_radius * sin(tmp), 0.0);
    }
    glEnd();
}

void WidgetOpenGL::resizeGL(int width, int height)
{
    //изменяем настройки порта вывода графики
    glViewport(0, 0, width, height);
    updateGL(); //обновляем
}
unsigned int WidgetOpenGL::pointsCount() const
{
    return m_pointsCount;
}

void WidgetOpenGL::setPointsCount(unsigned int pointsCount)
{
    m_pointsCount = pointsCount;
    updateGL(); //обновляем
}

float WidgetOpenGL::sizePointsAndLines() const
{
    return m_sizePointsAndLines;
}

void WidgetOpenGL::setSizePointsAndLines(float sizePointsAndLines)
{
    m_sizePointsAndLines = sizePointsAndLines;
    updateGL(); //обновляем
}

GLenum WidgetOpenGL::primitivesMode() const
{
    return m_primitivesMode;
}

void WidgetOpenGL::setPrimitivesMode(const QString& mode)
{
    if (mode == "GL_POINTS") {
        m_primitivesMode = GL_POINTS;
    } else if (mode == "GL_LINES") {
        m_primitivesMode = GL_LINES;
    } else if (mode == "GL_LINE_STRIP") {
        m_primitivesMode = GL_LINE_STRIP;
    } else if (mode == "GL_LINE_LOOP") {
        m_primitivesMode = GL_LINE_LOOP;
    } else if (mode == "GL_TRIANGLES") {
        m_primitivesMode = GL_TRIANGLES;
    } else if (mode == "GL_TRIANGLE_STRIP") {
        m_primitivesMode = GL_TRIANGLE_STRIP;
    } else if (mode == "GL_TRIANGLE_FAN") {
        m_primitivesMode = GL_TRIANGLE_FAN;
    } else if (mode == "GL_QUADS") {
        m_primitivesMode = GL_QUADS;
    } else if (mode == "GL_QUAD_STRIP") {
        m_primitivesMode = GL_QUAD_STRIP;
    } else if (mode == "GL_POLYGON") {
        m_primitivesMode = GL_POLYGON;
    }
    updateGL(); //обновляем
}

double WidgetOpenGL::radius() const
{
    return m_radius;
}

void WidgetOpenGL::setRadius(double radius)
{
    m_radius = radius;
    updateGL(); //обновляем
}
