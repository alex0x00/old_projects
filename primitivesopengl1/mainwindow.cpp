#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_spinBoxX_valueChanged(int arg1)
{
    ui->widget->move(QVector3D(arg1, 0, 0));
}

void MainWindow::on_spinBoxY_valueChanged(int arg1)
{
    ui->widget->move(QVector3D(0, arg1, 0));
}

void MainWindow::on_doubleSpinBoxR_valueChanged(double arg1)
{
    ui->widget->setRadius(arg1);
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    ui->widget->setPrimitivesMode(arg1);
}

void MainWindow::on_doubleSpinBoxSize_valueChanged(double arg1)
{
    ui->widget->setSizePointsAndLines(arg1);
}

void MainWindow::on_spinBoxCount_valueChanged(int arg1)
{
    ui->widget->setPointsCount(arg1);
}
