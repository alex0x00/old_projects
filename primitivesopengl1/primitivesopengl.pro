#-------------------------------------------------
#
# Project created by QtCreator 2014-06-11T14:34:20
#
#-------------------------------------------------


QT += opengl widgets
QT += core gui
TARGET = primitivesopengl
TEMPLATE = app
LIBS += -lopengl32

SOURCES += main.cpp\
        mainwindow.cpp \
    widgetopengl.cpp

HEADERS  += mainwindow.h \
    widgetopengl.h

FORMS    += mainwindow.ui
