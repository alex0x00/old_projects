#include <QApplication>
#include <QVector>
#include <QVector3D>
#include <QDebug>
#include <QtCore>
#include "glwidget.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    AnimationViewer window;

    window.show();

    return app.exec();
}
