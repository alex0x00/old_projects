#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtWidgets>
#include <QGLWidget>
#include "scene.h"

class AnimationViewer : public QOpenGLWidget {
    Q_OBJECT

public:
    explicit AnimationViewer(QWidget* parent = 0);
    ~AnimationViewer();

protected:
    void initializeGL();//инициализация opengl
    void paintGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);//обработчик нажатий
private:
    float m_aspectRatio;//отножение ширины высоты
    QVector3D m_position;//позиция камеры
    QVector3D m_center;//точка взгляда
    Scene m_scene;//наша сцена
    QTimer* m_timer;//таймер отрисовки
    QMatrix4x4 m_projection;//матрица проекции
    QMatrix4x4 m_view;//матрица точки обзора
    QMatrix4x4 m_viewTransform;
};

#endif
