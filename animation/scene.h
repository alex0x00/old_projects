#ifndef SCENE_H
#define SCENE_H
#include <QObject>
#include <QOpenGL.h>
#include <QTimer>
#include <QtOpenGL>
#include <math.h>
#include <qopengl.h>

class HeightMap { //класс карты высот (прямоугольная область из ячеек размера m_gridSize и со сторонами N M)
public:
    HeightMap();
    void load(const QString& path, QString textureName); //загрузка карты высот
    void draw(); //отрисовываем карты

    float gridSize() const; //размер ячейки карты (квадрат)
    void setGridSize(float gridSize);

    void clear(); //очищаем карту
    float levelHeight(unsigned int i, unsigned int j); //получение высоты по координтам в массиве
    void setLevelHeight(unsigned int i, unsigned int j, float value); //устонвка высоты  по координтам в массиве

    void setSize(unsigned int N, unsigned int M); //задает размерность карты
    void scale(float s);

    float max() const; //макс высота
    float min() const; //мин высота

    QSize size() //размер карты
    {
        return QSize(m_N, m_M);
    }

private:
    QString m_path;
    QVector<float> m_map;
    float m_gridSize; //размер ячейки
    //размерность карты
    //map size N*M
    unsigned int m_N;
    unsigned int m_M;
    float m_max;
    float m_min;
    GLuint m_texture;
};

GLuint createTexture(const QString& path); //создаёт рессурс текстуры

class Tree { //класс деревьев и кустов
public:
    Tree(const QString& src, const QVector3D& position);
    ~Tree();

    void draw();
    void setPosition(const QVector3D& position);

private:
    GLuint m_texture;
    QMatrix4x4 m_transform;
};

class Scene : public QObject { //класс сцены
    Q_OBJECT
public:
    Scene(QObject* parent = 0);
    ~Scene();

    QMatrix4x4 position(float t); //линейная интерполяция векторного пути

    void draw();
    void initialize(); //инициализация сцены создание, загрузка и расчеты

public slots:
    void updateAnimate(); //анимирует сцену, изменяет положение саней
    void playAnimate()
    {
        m_timer->start();
    }

    void pauseAnimate()
    {
        m_timer->stop();
    }
    void setToStartAnimate()
    {
        m_time = 0.0001;
    }

    void drawBox(float length, float size = 0.3); //рисует параллелепипед
    void drawSleigh(); //ресует сани

private:
    HeightMap m_map;
    QVector<QVector3D> m_movePath;
    QMatrix4x4 m_transform;
    QTimer* m_timer; //таймер рассчета анимации
    float m_lengthPath; //длина дороги
    float m_time; //время анимации
    QVector<Tree*> m_tree;
};

#endif // SCENE_H
