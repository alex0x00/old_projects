HEADERS = glwidget.h \
    scene.h
SOURCES = glwidget.cpp \
        main.cpp \
    scene.cpp
CONFIG += c++11

QT      += opengl
QT      += widgets

LIBS += -lopengl32

RESOURCES += \
    resource.qrc
