#include <QtWidgets>
#include <QtOpenGL>
#include <QSurfaceFormat>
#include "glwidget.h"

AnimationViewer::AnimationViewer(QWidget* parent)
    : QOpenGLWidget(parent)
{
    QSurfaceFormat fmt;//настройки формата opengl
    fmt.setSamples(4);
    setFormat(fmt);

    m_position = QVector3D(5, 5, 15);
    m_center = QVector3D(5, 5, 5);
    m_timer = new QTimer(this);
    QObject::connect(m_timer, SIGNAL(timeout()),
                     this, SLOT(update()));
    m_timer->setInterval(5);
    m_timer->start();
}

AnimationViewer::~AnimationViewer()
{
}

void AnimationViewer::initializeGL()
{
    m_viewTransform.setToIdentity();
    m_aspectRatio = width() / height();//отношения сторон
    m_projection.perspective(30, m_aspectRatio, 1, 100);//настройка перспективы

    m_view.lookAt(m_position,
                  m_center,
                  QVector3D(0, 1, 0));//настройка точки обзора
//разрешаем тест глубины
    glEnable(GL_DEPTH_TEST);
// устанавливаем параметры для того чтоб не было резких границ между прозрачным местом и текстурой:
    glAlphaFunc(GL_GREATER, 0.0);


    glEnable(GL_BLEND); //Включаем режим смешивания цветов
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //параметры смешивания

    m_scene.initialize();//инициализация сцены
}

void AnimationViewer::paintGL()
{
    glClearColor(0.3, 0.3, 0.3, 1.0);//цвет фона
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//очищаем буферы
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    QMatrix4x4 vp;

    vp = m_projection * m_view;//формируем итогувую матрицу проекции

    glMatrixMode(GL_PROJECTION);//меняем тип текущей матрицы на проекционную
    glLoadMatrixf(vp.data());//загрузка матрицы
    glMatrixMode(GL_MODELVIEW);//меняем тип текущей матрицы на модельную
    glLoadIdentity();
    m_scene.draw();//отрисовка сцены
}

void AnimationViewer::resizeGL(int width, int height)//обработчик изменения размера окна
{
    glViewport(0, 0, width, height);//обновлем порт вывода opengl
    m_aspectRatio = width / height;
    m_projection.setToIdentity();
    m_projection.perspective(60, m_aspectRatio, 1, 1000);//обновляем перспективу
    update();//обновляем (перерисовываем )
}

void AnimationViewer::keyPressEvent(QKeyEvent* event)//обработчик нажатий клавиш
{
    float step = 1.0f;
    //выходи по escape
    //управляем положением камеры
    if (event->key() == Qt::Key_Escape) {
        exit(0);
    } else if (event->key() == Qt::Key_2) {
        m_position += QVector3D(0.0, 0.0, step);
        m_center += QVector3D(0.0, 0.0, step);
    } else if (event->key() == Qt::Key_8) {
        m_position += QVector3D(0.0, 0.0, -step);
        m_center += QVector3D(0.0, 0.0, -step);
    } else if (event->key() == Qt::Key_4) {//вращение (направление взгляда) камеры по оси y
        m_viewTransform.setToIdentity();
        m_viewTransform.rotate(5, QVector3D(0.0, 1.0, 0.0));
        m_center = m_viewTransform* (m_center - m_position);
        m_center = m_center + m_position;
    } else if (event->key() == Qt::Key_6) {
        m_viewTransform.rotate(-5, QVector3D(0.0, 1.0, 0.0));
        m_center = m_viewTransform* (m_center - m_position);
        m_center = m_center + m_position;
    } else if (event->key() == Qt::Key_Up) { //вверх
        m_position += QVector3D(0.0, step, 0.0);
        m_center += QVector3D(0.0, step, 0.0);
    } else if (event->key() == Qt::Key_Down) { //вниз
        m_position += QVector3D(0.0, -step, 0.0);
        m_center += QVector3D(0.0, -step, 0.0);
    } else if (event->key() == Qt::Key_Left) {
        m_position += QVector3D(-step, 0.0, 0.0);
        m_center += QVector3D(-step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_Right) {
        m_position += QVector3D(step, 0.0, 0.0);
        m_center += QVector3D(step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_F1) { //пуск анимации
        m_scene.playAnimate();
    } else if (event->key() == Qt::Key_F2) { //приостановка анимации
        m_scene.pauseAnimate();
    } else if (event->key() == Qt::Key_F3) { //в начало
        m_scene.setToStartAnimate();
    }
    m_view.setToIdentity();
    //формируем вид камеры
    m_view.lookAt(m_position,
                  m_center,
                  QVector3D(0, 1, 0));
    update();
}
