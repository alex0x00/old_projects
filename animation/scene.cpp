#include "scene.h"
#include <QGLWidget>
HeightMap::HeightMap()
{
    m_N = m_M = 0;
    m_gridSize = 0.0f;
}

void HeightMap::load(const QString& path, QString textureName)
{ //загружает информацию о карте высот
    QString path_src = path;
    if (!QFile::exists(path_src)) { //если файл не существует
        qWarning() << "Map file not found:" << path_src;
        return;
    }
    m_path = path_src;
    QFile file(path_src);
    if (!file.open(QIODevice::ReadOnly)) { //пытаемся открыть на чтение
        qWarning() << "File not opened:";
    }
    clear();
    QTextStream in(file.readAll());
    unsigned int N, M;
    bool isEndRead = false; //закончили чтение
    float currentReadingValue;
    in >> N >> M;
    setSize(N, M);
    //считываем высоты
    for (unsigned int i = 0; i < m_N && !isEndRead; ++i) {
        for (unsigned int j = 0; j < m_M && !isEndRead; ++j) {
            isEndRead = in.atEnd();
            in >> currentReadingValue;
            setLevelHeight(i, j, currentReadingValue);
        }
    }

    m_texture = createTexture(textureName); //загружаем текстуру этой карты
}

void HeightMap::draw()
{
    float currentHeight[4]; //ячейка карты
    QVector3D baseColor(0.5, 0.5, 0.5); //начальный цвет в низине
    QVector3D currentColor;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texture);
    //обход вершин
    //    0--1
    //    |  |
    //    3--2
    //для реалистичности добавим изменение цвета от высоты
    glBegin(GL_QUADS);
    for (unsigned int i = 0; i < m_N - 1; ++i) {
        for (unsigned int j = 0; j < m_M - 1; ++j) {
            currentHeight[0] = levelHeight(i, j);
            currentHeight[1] = levelHeight(i, j + 1);
            currentHeight[2] = levelHeight(i + 1, j + 1);
            currentHeight[3] = levelHeight(i + 1, j);

            glTexCoord2f(0.0, 1.0);
            currentColor = baseColor + (QVector3D(1.0, 1.0, 1.0) * (currentHeight[0] - min()) / (max() - min()) * 0.5);
            glColor3f(currentColor.x(), currentColor.y(), currentColor.z());
            glVertex3f(m_gridSize * i, currentHeight[0], m_gridSize * j);

            glTexCoord2f(1.0, 1.0);
            currentColor = baseColor + (QVector3D(1.0, 1.0, 1.0) * (currentHeight[1] - min()) / (max() - min()) * 0.5);
            glColor3f(currentColor.x(), currentColor.y(), currentColor.z());
            glVertex3f(m_gridSize * i, currentHeight[1], m_gridSize * (j + 1));

            glTexCoord2f(1.0, 0.0);
            currentColor = baseColor + (QVector3D(1.0, 1.0, 1.0) * (currentHeight[2] - min()) / (max() - min()) * 0.5);
            glColor3f(currentColor.x(), currentColor.y(), currentColor.z());
            glVertex3f(m_gridSize * (i + 1), currentHeight[2], m_gridSize * (j + 1));

            glTexCoord2f(0.0, 0.0);
            currentColor = baseColor + (QVector3D(1.0, 1.0, 1.0) * (currentHeight[3] - min()) / (max() - min()) * 0.5);
            glColor3f(currentColor.x(), currentColor.y(), currentColor.z());
            glVertex3f(m_gridSize * (i + 1), currentHeight[3], m_gridSize * j);
        }
    }
    glEnd();
    glDisable(GL_TEXTURE_2D);
}

void HeightMap::clear() //очистка карты
{
    if (m_map.isEmpty()) {
        m_map.clear();
    }
    m_N = m_M = 0;
}

float HeightMap::levelHeight(unsigned int i, unsigned int j) //уровень высоты
{
    if (i < m_N && j < m_M) {
        return m_map[i * m_M + j];
    } else {
        qDebug() << "Error access for index["
                 << i << "][" << j << "]"; //ошибка доступа,
        //выход за границу массива
        return 0.0f;
    }
}

void HeightMap::setLevelHeight(unsigned int i, unsigned int j, float value)
//изменение уровеня высоты
{
    if (i < m_N && j < m_M) {
        m_map[i * m_M + j] = value;

        if (value > m_max) {
            m_max = value;
        } else if (value < m_min) {
            m_min = value;
        }
    } else {
        qDebug() << "Error access for index["
                 << i << "][" << j << "]"; //ошибка доступа,
        //выход за границу массива
    }
}

void HeightMap::setSize(unsigned int N, unsigned int M)
{
    if (N * M != m_N * m_M) {
        clear();
        m_map.resize(N * M);
    }
    m_N = N;
    m_M = M;
}

void HeightMap::scale(float s)
{
    for (unsigned int k = m_N * m_M, i = 0; i < k; ++i) {
        m_map[i] *= s;
    }
}

float HeightMap::max() const
{
    return m_max;
}

float HeightMap::min() const
{
    return m_min;
}

float HeightMap::gridSize() const
{
    return m_gridSize;
}

void HeightMap::setGridSize(float gridSize)
{
    m_gridSize = gridSize;
}

Scene::Scene(QObject* parent)
    : QObject(parent)
{
    m_time = 0.0;
    m_timer = new QTimer();
}

Scene::~Scene()
{
}

QMatrix4x4 Scene::position(float t) //расчет матрицы положения объекта на пути (m_movePath) в процента  (t * 100.0)
{
    QVector3D segment, segment1, segment2, segment3; //отрезок l=b-a
    float currentDistance = t * m_lengthPath; //сколько нужно пройти
    float sumDistance = 0.0; //растояние от начала до требуемого (t)
    float distance;
    int i = 0;

    for (; i < m_movePath.length() - 1; ++i) { //ищем отрезок где должен находиться наш объект
        segment = m_movePath[i + 1] - m_movePath[i]; //l=point2-point1

        if (sumDistance + segment.length() >= currentDistance) { //если нашли
            break;
        }
        sumDistance += segment.length();
    }
    distance = currentDistance - sumDistance; //получаем нужную длину на исследуемом отрезке
    segment.normalize(); //нормализуем вектор

    segment1 = m_movePath[i] + segment * distance; //перемещение по вектору направления
    segment2 = m_movePath[i] + segment * (distance * 1.1); //перемещаемся еще для получения
    QMatrix4x4 ret;
    ret.translate(segment1); //формируем перемещение

    float angle; //выщитываем текущий наклон склона
    angle = QVector3D::dotProduct(QVector3D(0.0, 0.0, 1.0), segment); //угол наклона к оси координат (скалярное произведение векторов)
    angle = acosf(angle) * 180.0 / M_PI; //находим угол в градусах
    ret.rotate(angle, QVector3D(1.0, 0.0, 0.0)); //поворачиваем

    return ret;
}

void Scene::draw() //рисуем сцену целеком
{
    glColor3f(0.3, 0.5, 0.7);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    m_map.draw();
    glColor3f(0.8, 0.2, 0.2);

    QMatrix4x4 transform;
    transform.translate(10.0, 4.75, 2.0);
    transform.scale(2.0);

    drawSleigh();

    for (int i = 0; i < m_tree.length(); ++i) { //рисуем растительность
        m_tree[i]->draw();
    }
}

void Scene::initialize()
{
    int treeCount = 4;
    m_map.setGridSize(2.0f);
    m_map.load(QString(":/map2.txt"), QString(":/texture_snow.png")); //загружаем объекты

    for (int j = 2; j < m_map.size().height() - 1; ++j) { //составляем маршрут саней
        m_movePath.push_back(QVector3D(
            m_map.gridSize() * 4.0, //4 линя из карты высот
            m_map.levelHeight(4, j), //4 линя из карты высот
            m_map.gridSize() * j));
    }
    m_lengthPath = 0.0; //длина пути
    for (int i = 0; i < m_movePath.length() - 1; ++i) { //считаем длину пути саней
        QVector3D length = m_movePath[i + 1] - m_movePath[i]; //l=b-a
        m_lengthPath += length.length();
    }

    m_transform = position(0.001); //положение саней вначале
    connect(m_timer, SIGNAL(timeout()), //таймер анимации
        this, SLOT(updateAnimate()));
    m_timer->setInterval(10);

    qsrand(1); //инициализация ГПЧ
    float x, y;
    float N = m_map.size().width();
    float M = m_map.size().height();

    for (int i = 0; i < treeCount; ++i) { //случайно раставляем растительность
        x = (float(qrand()) / float(RAND_MAX)) * N;
        y = (float(qrand()) / float(RAND_MAX)) * M;
        m_tree.push_back(
            new Tree(QString(":/tree.png"),
                QVector3D(x * m_map.gridSize(), m_map.levelHeight(x, y), y * m_map.gridSize())));
        x = ((float)qrand() / (float)RAND_MAX) * N;
        y = ((float)qrand() / (float)RAND_MAX) * M;
        m_tree.push_back(
            new Tree(QString(":/tree2.png"),
                QVector3D(x * m_map.gridSize(), m_map.levelHeight(x, y), y * m_map.gridSize())));
    }
}

void Scene::updateAnimate()
{
    float step = 1.0f / 1000.0f; //щаг анимации
    if (m_time < 1.0f) {
        m_time += step;
        m_transform = position(m_time); //пооложение саней в текущий момент времени
    } else {
        m_timer->stop();
    }
}

void Scene::drawBox(float length, float size) // рисуем прямоугольник
{
    glBegin(GL_QUADS);
    size /= 2.0;
    //ближняя грань
    glVertex3f(0.0, size, size);
    glVertex3f(length, size, size);
    glVertex3f(length, -size, size);
    glVertex3f(0.0, -size, size);
    //левая грань
    glVertex3f(0.0, size, -size);
    glVertex3f(0.0, size, size);
    glVertex3f(0.0, -size, size);
    glVertex3f(0.0, -size, -size);
    // задняя грань
    glVertex3f(0.0, size, -size);
    glVertex3f(length, size, -size);
    glVertex3f(length, -size, -size);
    glVertex3f(0.0, -size, -size);
    //правая грань
    glVertex3f(length, size, -size);
    glVertex3f(length, size, size);
    glVertex3f(length, -size, size);
    glVertex3f(length, -size, -size);

    //верх
    glVertex3f(0.0, size, -size);
    glVertex3f(length, size, -size);
    glVertex3f(length, size, size);
    glVertex3f(0.0, size, size);
    //низ
    glVertex3f(0.0, -size, -size);
    glVertex3f(length, -size, -size);
    glVertex3f(length, -size, size);
    glVertex3f(0.0, -size, size);
    glEnd();
}

void Scene::drawSleigh()
{
    glColor3f(0.5, 0.2, 0.2);
    QMatrix4x4 transform, tmp;
    tmp = m_transform;
    tmp.scale(0.2);
    tmp.rotate(-90, QVector3D(0.0, 1.0, 0.0));
    //рисуем стойки саней

    //ближняя пара
    transform.setToIdentity();
    transform.translate(QVector3D(-2.0, 0.0, 1.0));
    transform.rotate(90, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);

    transform.setToIdentity();
    transform.translate(QVector3D(2.0, 0.0, 1.0));
    transform.rotate(90, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);
    //дальняя пара
    transform.setToIdentity();
    transform.translate(QVector3D(-2.0, 0.0, -1.0));
    transform.rotate(90, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);

    transform.setToIdentity();
    transform.translate(QVector3D(2.0, 0.0, -1.0));
    transform.rotate(90, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);

    //рисуем лыжи
    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 0.0, -1.0));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.2);

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 0.0, 1.0));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.2);

    //нос

    transform.setToIdentity();
    transform.translate(QVector3D(3.0, 0.0, -1.0));
    transform.rotate(60, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);

    transform.setToIdentity();
    transform.translate(QVector3D(3.0, 0.0, 1.0));
    transform.rotate(60, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.2);

    glColor3f(0.2, 0.9, 0.1);
    //соединительные рейки
    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, -1.0));
    transform.rotate(-90, QVector3D(0.0, 1.0, 0.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.3);

    transform.setToIdentity();
    transform.translate(QVector3D(3.0, 2.0, -1.0));
    transform.rotate(-90, QVector3D(0.0, 1.0, 0.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(2.0, 0.3);

    //сидение

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, -1.0));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.3);

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, -0.5));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.3);

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, 0.0));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.3);

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, 0.5));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.3);

    transform.setToIdentity();
    transform.translate(QVector3D(-3.0, 2.0, 1.0));
    transform.rotate(0, QVector3D(0.0, 0.0, 1.0));
    transform = tmp * transform;
    glLoadMatrixf(transform.data());
    drawBox(6.0, 0.3);
}

Tree::Tree(const QString& src, const QVector3D& position)
{
    m_texture = createTexture(src);
    setPosition(position);
}

Tree::~Tree()
{
    glDeleteTextures(1, &m_texture);
}

void Tree::draw()
{
    float size = 2.0;
    //отрисовывает квадраты с текстурой
    glLoadMatrixf(m_transform.data());
    glDepthMask(false);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texture);

    //обход вершин
    //    1--2
    //    |  |
    //    0--3
    glColor3f(1.0, 1.0, 1.0);

    glBegin(GL_QUADS);

    glTexCoord2f(0.0, 0.0);
    glVertex3f(-size, -size, size);

    glTexCoord2f(0.0, 1.0);
    glVertex3f(-size, size, size);

    glTexCoord2f(1.0, 1.0);
    glVertex3f(size, size, -size);

    glTexCoord2f(1.0, 0.0);
    glVertex3f(size, -size, -size);
    glEnd();

    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-size, -size, -size);

    glTexCoord2f(0.0, 1.0);
    glVertex3f(-size, size, -size);

    glTexCoord2f(1.0, 1.0);
    glVertex3f(size, size, size);

    glTexCoord2f(1.0, 0.0);
    glVertex3f(size, -size, size);

    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glDepthMask(true);
}

void Tree::setPosition(const QVector3D& position) //установка координат
{
    QVector3D pos = position;
    m_transform.setToIdentity();
    pos += QVector3D(0.0, 2.05, 0.0); //корекция положения
    m_transform.translate(pos);
}

GLuint createTexture(const QString& path)
{
    //создаем текстуру с нашим изображением
    QImage* image = new QImage(path);
    qDebug() << image->hasAlphaChannel();
    qDebug() << "createTexture";
    QImage prepare;
    //преобразуем изображение в формат для opengl
    prepare = QGLWidget::convertToGLFormat(*image);

    delete image;
    GLuint texture;
    glGenTextures(1, &texture); //запрашиваем индекс текстуры
    //используем этот текстурный
    //индекс  как текущий
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    //передаем данные
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
        prepare.width(), prepare.height(),
        0, GL_RGBA, GL_UNSIGNED_BYTE, prepare.bits());

    //параметры фильтрации
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return texture;
}
